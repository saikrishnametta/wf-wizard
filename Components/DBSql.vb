Option Explicit On
'Option Strict On

Imports System
Imports System.Data
Imports System.Xml
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic


'***************************************************************
' Author : Sai Krishna Metta
'*Module            All primitive DB interactions, which define data layer. 
'*
'* Description:     This Class defines all core Database interactions
'******************************************************

Namespace WatchForce.Data

    Public Class DBSql

        'Private members
        Private mobjConnection As SqlConnection
        Private mblnDisposed As Boolean = False
        Private mstrUserId As String
        Private mstrModuleName As String
        Private mstrWorkingFolder As String
        Private mobjTransaction As SqlTransaction = Nothing
        'Private mstrInitialCatalog, mstrDataSourceName,
        Private mConnectionString As String
        Private objError As New EventLogger()
        Private Const EXCEPTION_MSG As String = "There was an error in the method. " _
            & "Please see the Application Log for details."

        '**********************************************************************************
        'Purpose : Public Instance Constructors
        'Inputs  : 
        '		DatabaseName  : UserId
        '**********************************************************************************
        Public Sub New()
            'Initialize private members
            mobjConnection = Nothing
            mstrUserId = Nothing
            mstrModuleName = Me.GetType.ToString
        End Sub

        Public Sub New(ByVal UserId As String)
            mstrUserId = UserId
            mConnectionString = String.Empty
            'Dim connString = ConfigurationManager.ConnectionStrings("WFConnString").ConnectionString
            OpenConnection()
            mstrModuleName = Me.GetType.ToString
        End Sub

        Public Sub New(ByVal UserId As String, _
                       ByVal strWorkingFolder As String)
            mstrUserId = UserId
            mstrWorkingFolder = strWorkingFolder
            mConnectionString = String.Empty
            OpenConnection()
            mstrModuleName = Me.GetType.ToString
        End Sub

        '**********************************************************************************
        'Purpose : Public Instance Properties
        '**********************************************************************************

        Public Property DBConnection() As String
            Set(ByVal strValue As String)
                If Not mobjConnection Is Nothing Then
                    Dispose()
                End If
                mConnectionString = String.Empty
                OpenConnection()
            End Set
            Get
                Try
                    Return mobjConnection.ConnectionString
                Catch
                    Return ""
                End Try
            End Get
        End Property

        Public Property UserId() As String
            Set(ByVal UserId As String)
                mstrUserId = UserId
                If Not mobjConnection Is Nothing Then
                    Dispose()
                End If
                OpenConnection()
            End Set
            Get
                Return mstrUserId
            End Get
        End Property


        Public Property ApplicationPath() As String
            Set(ByVal strApplicationPath As String)
                mstrWorkingFolder = strApplicationPath
            End Set
            Get
                Return mstrWorkingFolder
            End Get
        End Property


        'Public Methods
        '**********************************************************************************
        'Purpose : BeginTrans,CommitTrans and RollBackTrans manage the Global
        '        Transaction object mobjTransaction
        '**********************************************************************************
        Public Function BeginTrans() As Boolean
            Try
                mobjTransaction = mobjConnection.BeginTransaction
                Return True
            Catch objException As Exception
                CatchBlock(objException)
            End Try
        End Function

        Public Function CommitTrans() As Boolean
            Try
                mobjTransaction.Commit()
                mobjTransaction = Nothing
                Return True
            Catch objException As Exception
                CatchBlock(objException)
            End Try
        End Function

        Public Function RollbackTrans() As Boolean
            Try
                mobjTransaction.Rollback()
                Return True
            Catch objException As Exception
                CatchBlock(objException)
            End Try
        End Function

        Public Function GetConnectionObject() As SqlConnection
            Return mobjConnection
        End Function

        '**********************************************************************************
        'Purpose : All fields are inserted .
        'Inputs  : 
        '		TableName : The Table to insert into
        '       strValues : The values to be inserted
        '**********************************************************************************
        Public Function insertValues(ByVal TableName As String, _
                                     ByVal strValues As String) As Boolean
            Dim strinsSql As String = ""
            Try
                insertValues = False
                'Make sure that the object has not been disposed yet
                If mblnDisposed = True Then
                    Throw New ObjectDisposedException(mstrModuleName, _
                          "This object has already been disposed.")
                End If
                strinsSql = "INSERT INTO " & TableName & " values(" & strValues & ")"
                insertValues = ExecuteStatement(strinsSql)
            Catch objException As Exception
                insertValues = False
                LogError(objException, strinsSql)
            End Try

        End Function

        '**********************************************************************************
        'Purpose : Inserts selected fields only
        'Inputs  : 
        '		TableName : The Table to insert into
        '       strFields : The Fields to be inserted
        '       strValues : The values to be inserted
        '**********************************************************************************
        Public Function insertValues(ByVal TableName As String, _
                                        ByVal strFields As String, _
                                        ByVal strValues As String) As Boolean
            Dim strinsSql As String
            Try

                insertValues = False
                'Make sure that the object has not been disposed yet
                If mblnDisposed = True Then
                    Throw New ObjectDisposedException(mstrModuleName, _
                          "This object has already been disposed.")
                End If
                strinsSql = "INSERT INTO " & TableName & "(" & strFields & ") values(" & strValues & ")"
                insertValues = ExecuteStatement(strinsSql)
            Catch objException As Exception
                insertValues = False
                LogError(objException, strinsSql)
            End Try
        End Function

        Public Function insertValues(ByVal strInsertStmt As String) As Boolean
            Dim strinsSql As String
            Try

                insertValues = False
                'Make sure that the object has not been disposed yet
                If mblnDisposed = True Then
                    Throw New ObjectDisposedException(mstrModuleName, _
                          "This object has already been disposed.")
                End If
                strinsSql = strInsertStmt
                insertValues = ExecuteStatement(strinsSql)
            Catch objException As Exception
                insertValues = False
                LogError(objException, strinsSql)
            End Try
        End Function

        '**********************************************************************************
        'Purpose : Deletes from database
        'Inputs  : 
        '		TableName : The Table to delete from
        '       condition : to delete on 
        '**********************************************************************************
        Public Function delete(ByVal TableName As String, _
                                ByVal Condition As String) As Boolean
            Dim strdelSql As String
            Try
                delete = False
                'Make sure that the object has not been disposed yet
                If mblnDisposed = True Then
                    Throw New ObjectDisposedException(mstrModuleName, _
                          "This object has already been disposed.")
                End If
                strdelSql = "Delete from " & TableName & " where " & Condition
                delete = ExecuteStatement(strdelSql)
            Catch objException As Exception
                delete = False
                LogError(objException, strdelSql)
            End Try

        End Function

        '**********************************************************************************
        'Purpose : Updates the database
        'Inputs  : 
        '		TableName : The Table to update
        '       Condition : To update on 
        '**********************************************************************************
        Public Function update(ByVal TableName As String, _
                               ByVal Condition As String) As Boolean
            Dim strupdSql As String
            Try
                update = False
                'Make sure that the object has not been disposed yet
                If mblnDisposed = True Then
                    Throw New ObjectDisposedException(mstrModuleName, _
                          "This object has already been disposed.")
                End If
                strupdSql = "Update " & TableName & " set " & Condition
                update = ExecuteStatement(strupdSql)
            Catch objException As Exception
                update = False
                LogError(objException, strupdSql)
            End Try
        End Function


        '**********************************************************************************
        'Purpose : Selects from database certain fields
        'Inputs  : 
        '		TableName : The Table to select from
        '       objReturn : (ByRef) To be filled . Can be a dataset or dataReader .
        '       strTable  : If this has a value passed in the object returned is a dataset 
        '                   with the table name as strTable . If empty then a datareader is returned.
        '       strCondition : The selection Condition .
        '       strFields   : Fields to be returned 
        '**********************************************************************************
        Public Sub getInfoFields(ByVal TableName As String, ByRef objReturn As Object, _
                               ByVal strFields As String, Optional ByVal strTable As String = "", _
                               Optional ByVal strCondition As String = "")
            Dim strSql As String
            Try
                If (strCondition = "") Then
                    strSql = "select " & strFields & " from " & TableName
                Else
                    strSql = "select " & strFields & " from " & TableName & " where " & strCondition
                End If
                If (strTable <> "") Then
                    Dim objCmnd As New SqlDataAdapter()
                    objCmnd.SelectCommand = New SqlCommand()
                    objCmnd.SelectCommand.CommandText = strSql
                    objCmnd.SelectCommand.Connection = mobjConnection
                    If Not (mobjTransaction Is Nothing) Then
                        objCmnd.SelectCommand.Transaction = mobjTransaction
                    End If
                    objCmnd.Fill(objReturn, strTable)
                    objCmnd.Dispose()
                Else
                    Dim objCmnd As New SqlCommand(strSql, mobjConnection)
                    If Not (mobjTransaction Is Nothing) Then
                        objCmnd.Transaction = mobjTransaction
                    End If
                    objReturn = objCmnd.ExecuteReader()
                    objCmnd.Dispose()
                End If

            Catch objException As Exception
                LogError(objException, strSql)
            End Try

        End Sub
        '**********************************************************************************
        'Purpose : Selects from database one single value
        'Inputs  : 
        '		TableName : The Table to select from 
        '       strCondition : The selection Condition .
        '       strFields   : Fields to be returned 
        'Output  : 
        '       objReturn : Of generic type Object 
        '**********************************************************************************
        Public Function ExecuteStoredProc(ByVal StoredProcName As String, ByVal strFields As String)

            Try
                Dim sqlConnection1 As SqlClient.SqlConnection
                Dim cmd As New SqlClient.SqlCommand
                Dim reader As SqlClient.SqlDataReader

                sqlConnection1 = New SqlClient.SqlConnection
                sqlConnection1 = mobjConnection
                cmd.CommandText = StoredProcName
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = sqlConnection1
                If strFields <> "" Then
                    cmd.Parameters.Add(New SqlClient.SqlParameter("@Payrollid", SqlDbType.BigInt, 10))
                    'cmd.Parameters.Add(New SqlClient.SqlParameter("@Company", SqlDbType.VarChar, 10))
                    cmd.Parameters("@Payrollid").Value = CInt(strFields)
                    'cmd.Parameters("@Company").Value = company
                End If

                If Not (mobjTransaction Is Nothing) Then
                    cmd.Transaction = mobjTransaction
                End If
                reader = cmd.ExecuteReader()
            Catch objException As Exception
                LogError(objException, StoredProcName)
            End Try

        End Function



        '**********************************************************************************
        'Purpose : Selects from database one single value
        'Inputs  : 
        '		TableName : The Table to select from 
        '       strCondition : The selection Condition .
        '       strFields   : Fields to be returned 
        'Output  : 
        '       objReturn : Of generic type Object 
        '**********************************************************************************
        Public Function getInfoFieldsRetVal(ByVal TableName As String, _
                               ByVal strFields As String, _
                               Optional ByVal strCondition As String = "") As Object
            Dim strSql As String
            Dim objReturn As Object
            Try
                strSql = "select " & strFields & " from " & TableName
                If strCondition <> "" Then strSql = strSql & " where " & strCondition
                Dim objCmnd As New SqlCommand(strSql, mobjConnection)
                If Not (mobjTransaction Is Nothing) Then
                    objCmnd.Transaction = mobjTransaction
                End If
                objCmnd.CommandTimeout = 300
                objReturn = objCmnd.ExecuteScalar()
                objCmnd.Dispose()
                Return objReturn
            Catch objException As Exception
                LogError(objException, strSql)
            End Try
        End Function

        Public Function getInfoFieldsRetVal(ByVal strSql As String) As Object
            Dim objReturn As Object
            Try
                Dim objCmnd As New SqlCommand(strSql, mobjConnection)
                If Not (mobjTransaction Is Nothing) Then
                    objCmnd.Transaction = mobjTransaction
                End If
                objCmnd.CommandTimeout = 300
                objReturn = objCmnd.ExecuteScalar()
                objCmnd.Dispose()
                Return objReturn
            Catch objException As Exception
                LogError(objException, strSql)
            End Try
        End Function

        '**********************************************************************************
        'Purpose : Gets count of table
        'Inputs  : 
        '		TableName : The Table to count
        '       strCondition : The  Condition for count command.
        'Output  : 
        '       Count of rows in table matching optionnal condition
        '**********************************************************************************
        Public Function getCount(ByVal TableName As String, _
                                Optional ByVal strCondition As String = "") As Integer
            Dim strSql As String

            Try
                If (Trim(strCondition) = "") Then
                    strSql = "select count(*) from " & TableName
                Else
                    strSql = "select count(*) from " & TableName & " where " & strCondition
                End If
                Dim objCmnd As New SqlCommand(strSql, mobjConnection)
                If Not (mobjTransaction Is Nothing) Then
                    objCmnd.Transaction = mobjTransaction
                End If
                Return objCmnd.ExecuteScalar()
                objCmnd.Dispose()
            Catch objException As Exception
                LogError(objException, strSql)
            End Try
        End Function

        Public Sub getSQL(ByVal strSql As String, ByRef objReturn As Object, _
                                Optional ByVal strTable As String = "")
            Try
                If (strTable <> "") Then
                    Dim objCmnd As New SqlDataAdapter()
                    objCmnd.SelectCommand = New SqlCommand()
                    objCmnd.SelectCommand.CommandText = strSql
                    objCmnd.SelectCommand.Connection = mobjConnection
                    objCmnd.SelectCommand.CommandTimeout = 0
                    If Not (mobjTransaction Is Nothing) Then
                        objCmnd.SelectCommand.Transaction = mobjTransaction
                    End If
                    objCmnd.Fill(objReturn, strTable)

                    objCmnd.Dispose()
                    objCmnd = Nothing
                Else
                    Dim objCmnd As New SqlCommand(strSql, mobjConnection)
                    objCmnd.CommandTimeout = 0
                    If Not (mobjTransaction Is Nothing) Then
                        objCmnd.Transaction = mobjTransaction
                    End If
                    objReturn = objCmnd.ExecuteReader()
                    objCmnd.Dispose()
                End If
            Catch Ex As Exception
                LogError(Ex, strSql & ", for companyURL= " & mstrUserId)
            End Try
        End Sub

        Public Sub getDataTable(ByVal strSql As String, ByRef objTable As Object)
            Try
                Dim objCmnd As New SqlDataAdapter()
                objCmnd.SelectCommand = New SqlCommand()
                objCmnd.SelectCommand.CommandText = strSql
                objCmnd.SelectCommand.Connection = mobjConnection
                objCmnd.SelectCommand.CommandTimeout = 0
                If Not (mobjTransaction Is Nothing) Then
                    objCmnd.SelectCommand.Transaction = mobjTransaction
                End If
                objCmnd.Fill(objTable)
                objCmnd.Dispose()
                objCmnd = Nothing
            Catch objException As Exception
                LogError(objException, strSql)
            End Try
        End Sub


        '**********************************************************************************
        'Purpose : Disposes connection object . Should be used by application after
        '           Using other API's
        '**********************************************************************************
        Public Sub Dispose()
            If mblnDisposed = False Then
                Try
                    If Not (mobjConnection Is Nothing) Then
                        mobjConnection.Close()
                        'Free up the database connection resource by 
                        'calling its Dispose method
                        mobjConnection.Dispose()
                    End If
                Catch objException As Exception
                    LogError(objException, "")
                Finally
                    'Because this Dispose method has done the necessary cleanup,
                    'prevent the Finalize method from being called.
                    GC.SuppressFinalize(Me)
                    'Let our class know that Dispose() has been called
                    mblnDisposed = True
                End Try
            End If
        End Sub

        'Private/Protected Methods 
        '**********************************************************************************
        'Purpose : Opens Connection 
        'Inputs  : 
        '		strConn : The connection string
        '**********************************************************************************
        Private Function OpenConnection() As Boolean
            Try
                OpenConnection = False
                If Me.mConnectionString.Trim.Length = 0 Then
                    getConnectionValues()
                Else
                    'ParseConnString(strConn)
                End If
                mobjConnection = New SqlConnection(Me.mConnectionString)
                mobjConnection.Open()
                OpenConnection = True
                Me.mblnDisposed = False
            Catch objException As Exception
                'Log the error to the Application Log.
                LogError(objException, " strConn:" & Me.mConnectionString)
                'Throw a new exception, with the original exception nested.
                Throw New Exception(EXCEPTION_MSG & " strConn:" & Me.mConnectionString, objException)
            Finally

            End Try
        End Function

        'Private Methods 
        '**********************************************************************************
        'Purpose : Parses the connection string to retrieve Provider,url,data source
        'Inputs  : Connection String
        '		
        '**********************************************************************************
        'Private Sub ParseConnString(ByVal strConn As String)
        '    Dim strTemp As String
        '    'Set provider
        '    mstrProviderName = GetConstantValue("PROVIDER", strConn)
        '    'Set Url
        '    strTemp = GetConstantValue("PASSWORD", strConn)
        '    mstrCompanyURL = strTemp.Substring(strTemp.IndexOf("_") + 1)
        '    'Set Data Source
        '    mstrDataSourceName = GetConstantValue("SOURCE", strConn)
        'End Sub

        'Private Methods 
        '**********************************************************************************
        'Purpose : Retrieves a value from a connection string
        'Inputs  : 
        '       strToCheck : The string whose value is needed
        '		strConn : The connection String
        '**********************************************************************************
        'Private Function GetConstantValue(ByVal strToCheck As String, _
        '                             ByVal strConn As String) As String
        '    Dim strCopy As String = UCase(strConn)
        '    Dim index, sepIndex As Integer
        '    index = -1
        '    sepIndex = -1
        '    index = strCopy.IndexOf(strToCheck)
        '    index += strToCheck.Length
        '    index += strCopy.Substring(index).IndexOf("=")
        '    index += 1
        '    sepIndex = strCopy.Substring(index).IndexOf(";")
        '    Return strCopy.Substring(index, sepIndex)
        'End Function

        'Private/Protected Methods 
        '**********************************************************************************
        'Purpose : GetConnectionDetail 
        'Inputs  : 
        '		
        '**********************************************************************************
        Private Function getConnectionValues() As Boolean
            Try
                Dim connString = ConfigurationManager.ConnectionStrings("WFConnString").ConnectionString
                If connString <> String.Empty Then
                    Me.mConnectionString = connString
                End If

                getConnectionValues = True
            Catch ex As Exception

            End Try

            'Dim xmlConfigurationFile As New System.Xml.XmlDocument()
            'Dim strFullFileName As String = ""
            'Dim PathNode As System.Xml.XmlNode
            'Try

            '    getConnectionValues = False
            '    'Me.mstrInitialCatalog = "MFW"
            '    'Me.mstrDataSourceName = "192.168.9.68"
            '    'objError.WriteLog("GetExecutingAssembly.Location" & System.Reflection.Assembly.GetExecutingAssembly.Location)
            '    'objError.WriteLog("Application.StartUpPath" & System.Windows.Forms.Application.StartupPath)
            '    'objError.WriteLog("Application.ExecutablePath()" & System.Windows.Forms.Application.ExecutablePath())

            '    'If mstrWorkingFolder = "" Then Exit Function

            '    strFullFileName = "\WForce.config"
            '    xmlConfigurationFile.Load(strFullFileName)
            '    PathNode = xmlConfigurationFile.SelectSingleNode("//appSettings/add[@key='ConnectionString']")
            '    If Not PathNode.Attributes("value").Value Is Nothing Then
            '        Me.mConnectionString = PathNode.Attributes("value").Value
            '    End If

            '    'PathNode = xmlConfigurationFile.SelectSingleNode("//appSettings/add[@key='dataSource']")

            '    'If Not PathNode.Attributes("value").Value Is Nothing Then
            '    '    Me.mstrDataSourceName = PathNode.Attributes("value").Value
            '    'End If

            '    getConnectionValues = True
            'Catch objException As Exception
            '    'Log the error to the Application Log.
            '    LogError(objException, " FullFileName:" & strFullFileName)
            '    'Throw a new exception, with the original exception nested.
            '    'Throw New Exception(EXCEPTION_MSG & " strConn:" & FullFileName, objException)
            'Finally
            '    xmlConfigurationFile = Nothing
            'End Try
        End Function

        '**********************************************************************************
        'Purpose : Executes a sql statement
        'Inputs  : 
        '		strSql : Statement to be executed
        '**********************************************************************************
        Public Function ExecuteStatement(ByVal strSql As String) As Boolean
            Try
                ExecuteStatement = False
                Dim objCmnd As New SqlCommand(strSql, mobjConnection)
                objCmnd.CommandTimeout = 0
                objCmnd.Transaction() = mobjTransaction
                objCmnd.ExecuteNonQuery()
                ExecuteStatement = True
            Catch objException As Exception
                ExecuteStatement = False
                LogError(objException, strSql)
            End Try
        End Function

        '**********************************************************************************
        'Purpose : Handles the Catch block for many functions
        'Inputs  : 
        '		objException : Exception to be proccesed .
        '**********************************************************************************
        Private Sub CatchBlock(ByVal objException As Exception, _
                                Optional ByVal strMessage As String = "")
            'Write the error to the event Log
            LogError(objException, strMessage)
            'Close the Connection.
            mobjConnection.Close()
            'Throw a new exception, with the original exception nested.
            'Throw New Exception(EXCEPTION_MSG, objException)
        End Sub

        'Provides a safeguard in case Dispose does not get called.
        Protected Overrides Sub Finalize()
            Try
                'Dispose()
            Catch objException As Exception
                LogError(objException, "")
            Finally

            End Try
        End Sub

        '**********************************************************************************
        'Purpose : Writes error info to the event log .
        'Inputs  : 
        '		objException : Exception to be proccesed .
        '**********************************************************************************
        Private Sub LogError(ByRef objException As Exception, _
                                Optional ByVal strMessage As String = "")
            'Log the error information to the Application Log
            Dim strLogMsg As String
            'Dim objError As New EventLogger()
            Try

                strLogMsg = "An error occurred in the following module: " _
                & mstrModuleName & vbCrLf _
                & "strMessage : " & strMessage _
                & "Message: " & objException.Message & vbCrLf _
                & "Stack Trace:  " & objException.StackTrace & vbCrLf _
                & "Target Site : " & objException.TargetSite.ToString() & vbCrLf

                objError.WriteLog(strLogMsg)
            Catch
            Finally
                'objError = Nothing
            End Try
        End Sub

#Region "Sql Command,Procedure call with getting data table"

        Public Sub CreateSQLCommand(ByRef objCmd As SqlCommand)
            Try
                objCmd.Connection = mobjConnection
                objCmd.CommandTimeout = 0
                If Not (mobjTransaction Is Nothing) Then
                    objCmd.Transaction = mobjTransaction
                End If
            Catch ex As Exception

            End Try
        End Sub
        '**********************************************************************************
        'Purpose : Creates a command object byref with procedure specified .
        'Inputs  : 
        '		Sql Command,Procedure Name .
        '**********************************************************************************
        Public Sub CreateSQLCommand(ByRef objCmd As SqlCommand, ByVal SqlCommandName As String, Optional ByVal TimeOutPeriod As Integer = 0)
            Try
                objCmd.CommandText = SqlCommandName
                objCmd.CommandType = CommandType.StoredProcedure
                objCmd.Connection = mobjConnection
                objCmd.CommandTimeout = TimeOutPeriod
                If Not (mobjTransaction Is Nothing) Then
                    objCmd.Transaction = mobjTransaction
                End If
            Catch ex As Exception

            End Try
        End Sub
        '**********************************************************************************
        'Purpose : Creates parameters for the specified procedure .
        'Inputs  : 
        '		Sql Command,Param Name,Param Value .
        '**********************************************************************************
        Public Sub CreateParameter(ByRef objCmd As SqlCommand, ByVal strParamName As String, _
                                    ByVal strValue As String)
            Try
                Dim objParam As New SqlParameter()
                objParam.ParameterName = strParamName
                objParam.Value = strValue

                objCmd.Parameters.Add(objParam)

                objParam = Nothing

            Catch objException As Exception
                LogError(objException, objCmd.CommandText)
            End Try
        End Sub
        '**********************************************************************************
        'Purpose : Gives a result with the procedure mentioned as a data table
        'Inputs  : 
        '		Sql Command,Data Table as reference variables
        '**********************************************************************************

        Public Sub getDataTableFromSP(ByVal objCmd As SqlCommand, ByRef objTable As Object)
            Try
                Dim objAdapter As New SqlDataAdapter()
                objAdapter.SelectCommand = objCmd
                objAdapter.SelectCommand.Connection = mobjConnection
                objAdapter.SelectCommand.CommandTimeout = 0
                If Not (mobjTransaction Is Nothing) Then
                    objAdapter.SelectCommand.Transaction = mobjTransaction
                End If
                objAdapter.Fill(objTable)
                objAdapter.Dispose()
                objAdapter = Nothing
            Catch objException As Exception
                LogError(objException, objCmd.CommandText)
            End Try
        End Sub
#End Region

    End Class

End Namespace
