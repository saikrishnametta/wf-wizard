Imports System
Imports System.IO
Imports System.Diagnostics
Imports Microsoft.VisualBasic

'***************************************************************
' Author : Sai Krishna Metta
'*Module            Error trapping Component 
'*
'* Description:     This Class is to write a event, 
'*                   writing to file is optional
'******************************************************

Namespace WatchForce.Data
    Public Class EventLogger

        Public Const ERR_MSG = "<img style=""cursor:help;"" src=""images/err_indicator.gif"""

        'constants for EventLog
        Const EVENT_LOG As String = "WatchForce"
        Const DEFAULT_SOURCE As String = "SIXApplication"
        Const DEFAULT_CATEGORY As Short = 1
        Const DEFAULT_EVENTID As Integer = 1
        'Constants for Error file log
        Const SEVERITY_HEADER As String = " SEVERITY: "
        Const MESSAGE_HEADER As String = " MESSAGE: "


        'class-level variables
        Private bFileFlag As Boolean = False
        'Class level variables for Error file log
        Dim mFileName As String = "ErrLog_" & Format(Today(), "MMDDYYYY") & ".txt"
        Dim mPath As String = Environment.CurrentDirectory

        '********************************************************
        '* Constructors
        '********************************************************
        Public Sub New()
            bFileFlag = False
            mFileName = "ErrLog_" & Format(Today(), "MMDDYYYY") & ".txt"
        End Sub
        'default constructor
        Public Sub New(ByVal FileSave As Boolean)
            bFileFlag = FileSave
            mFileName = "ErrLog_" & Format(Today(), "MMDDYYYY") & ".txt"
        End Sub

        Public Sub Dispose()
            Try
                'to release the memory
            Catch
                '
            Finally
                'nothing in this case
            End Try
        End Sub

        '********************************************************
        '* Methods
        '********************************************************
        'This method will write the event in the EventLog
        'and if the bFileFlag is set then it will even call the
        'WriteFileLog sunroutine
        Public Sub WriteLog(ByVal strMessage As String, Optional ByVal LogType As EventLogEntryType = EventLogEntryType.Information, _
                            Optional ByVal Category As Short = DEFAULT_CATEGORY, _
                            Optional ByVal EventID As Integer = DEFAULT_EVENTID, _
                            Optional ByVal Source As String = DEFAULT_SOURCE)

            Dim cbEventLog As New System.Diagnostics.EventLog()
            Try
                'Dim cbEventLog As New System.Diagnostics.EventLog()
                'if it doesn't already exist create a source for the event to be logged to
                If Not (cbEventLog.SourceExists(Source)) Then
                    cbEventLog.CreateEventSource(Source, EVENT_LOG)
                End If
                cbEventLog.WriteEntry(Source, strMessage, LogType)
                If CBool(bFileFlag) Then WriteFileLog(strMessage, LogType)
            Catch ex As Exception
                'catch a general exception and pass back to caller
                cbEventLog.WriteEntry("HannaRunTime", strMessage, EventLogEntryType.Information)
                cbEventLog.WriteEntry("HannaRunTime", ex.Message(), EventLogEntryType.Error)
                'cbEventLog.Close()
            Finally
                If Not cbEventLog Is Nothing Then
                    cbEventLog.Close()
                    cbEventLog.Dispose()
                End If
                cbEventLog = Nothing
            End Try
        End Sub

        'This is the implementation of the ILog.WriteLog method
        Public Sub WriteFileLog(ByVal strMessage As String, ByVal LogType As EventLogEntryType)

            Dim OutputStream As StreamWriter
            Try
                'create the StreamWriter class which is part of the System.IO namespace
                OutputStream = New StreamWriter(mPath + Path.DirectorySeparatorChar + mFileName, True, System.Text.Encoding.Default)

                OutputStream.WriteLine(DateTime.Now & SEVERITY_HEADER & LogType.ToString() & MESSAGE_HEADER & strMessage)
            Catch ex As Exception
                'catch a general exception and pass back to caller
                'Throw ex
                Dim cbEventLog As New System.Diagnostics.EventLog()
                cbEventLog.WriteEntry(ex.Message(), EventLogEntryType.Error)
                cbEventLog.Close()

            Finally
                'regardless of what happens we want to close the stream
                OutputStream.Close()
            End Try
        End Sub

        '**********************************************************************************
        'Purpose : Writes error info to the event log .
        'Inputs  : 
        '		objException : Exception to be proccesed .
        '**********************************************************************************
        Public Sub LogError(ByRef objException As Exception, _
                            Optional ByVal strModuleName As String = "", _
                            Optional ByVal strMessage As String = "")
            'Log the error information to the Application Log
            Dim strLogMsg As String

            Try

                strLogMsg = "An error occurred in the following module: " _
                & strModuleName & vbCrLf _
                & "strMessage : " & strMessage _
                & "Message: " & objException.Message & vbCrLf _
                & "Stack Trace:  " & objException.StackTrace & vbCrLf _
                & "Target Site : " & objException.TargetSite.ToString() & vbCrLf

                WriteLog(strLogMsg)

            Catch
            Finally

            End Try
        End Sub
    End Class
End Namespace