﻿Imports WF.WatchForce.Data

Partial Public Class _Default
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtORG As DataTable
    Dim DBConn As DBSql

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")

            If Not Page.IsPostBack Then
                dtORG = LoadORGs()

            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Function LoadORGs() As DataTable
        Try
            dtORG = New DataTable()
            Return dtORG
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
        Return New DataTable()
    End Function
   
End Class