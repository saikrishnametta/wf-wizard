﻿Imports WF.WatchForce.Data
Imports System.Data.SqlClient

Partial Public Class DiscriminationDetails
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql
    Dim ComplaintID, ContactID As Int64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")

            CalendarRecentDesDt.EndDate = DateTime.Today

            If Not Page.IsPostBack Then
                LoadDropdowns()
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadDropdowns()
        Try
            LoadDropdown(ddlRace, "Race")
            LoadDropdown(ddlNatOrigin, "NationalOrigin")
            'LoadDropdown(ddlSex, "Sex")
            txtGender.Text = "Male"
            LoadDropdown(ddlSexOrientation, "SexOrientation")
            LoadDropdown(ddlGenderIdentity, "GenderIdentity")
            LoadDropdown(ddlPregnancy, "Pregnancy")
            LoadDropdown(ddlReligion, "Religion")
            LoadDropdown(ddlColor, "Color")
            LoadDropdown(ddlRetaliation, "Retaliation")
            LoadDropdown(ddlDisabilityHistory, "DisabilityHistory")
            LoadDropdown(ddlWorkingConditions, "WorkingConditions")
            LoadDropdown(ddlMedicalRecords, "MedicalRecords")
            LoadDropdown(ddlHowRequestMade, "HowRequestMade")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadDropdown(ByRef ddl As System.Web.UI.WebControls.DropDownList, ByVal ddlCode As String)
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spGetDiscriminationDdl"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Dropdown"
            param.DbType = DbType.String
            param.Value = ddlCode
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                ddl.DataSource = dtItems
                ddl.DataValueField = "Code"
                ddl.DataTextField = "Description"
                ddl.DataBind()
            End If
            ddl.Items.Insert(0, New ListItem("Please Select", -1))

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub


    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProceed.Click
        Try
            saveDiscriminationDetails()
            Response.Redirect("HarassmentDetails.aspx", False)
        Catch ex As Exception

        End Try
    End Sub


    Private Sub saveDiscriminationDetails()

        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter

        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spAddDiscrimination"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintID"
            param.DbType = DbType.Int64
            param.Value = Session("ComplaintID")
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RaceValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlRace.SelectedValue = "-1", System.DBNull.Value, ddlRace.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RaceValueOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtRaceOther.Text.Trim() <> String.Empty, txtRaceOther.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@NationalOriginID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlNatOrigin.SelectedValue = "-1", System.DBNull.Value, ddlNatOrigin.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@NationalOriginOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtNatOriginOthers.Text.Trim() <> String.Empty, txtNatOriginOthers.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            'LKP_GenderTypeCodes Discrimination based on Sex valueid 67
            param = New SqlClient.SqlParameter()
            param.ParameterName = "@SexValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(chkGender.Checked, 67, System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@SexOrientationValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlSexOrientation.SelectedValue = "-1", System.DBNull.Value, ddlSexOrientation.SelectedValue)
            cmd.Parameters.Add(param)
            param = New SqlClient.SqlParameter()
            param.ParameterName = "@SexOrientationValueOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtSexOrientationOthers.Text.Trim() <> String.Empty, txtSexOrientationOthers.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@GenderidentityValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlGenderIdentity.SelectedValue = "-1", System.DBNull.Value, ddlGenderIdentity.SelectedValue)
            cmd.Parameters.Add(param)
            param = New SqlClient.SqlParameter()
            param.ParameterName = "@GenderidentityValueOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtGenderIdentityOthers.Text.Trim() <> String.Empty, txtGenderIdentityOthers.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@PregnancyValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlPregnancy.SelectedValue = "-1", System.DBNull.Value, ddlPregnancy.SelectedValue)
            cmd.Parameters.Add(param)
            param = New SqlClient.SqlParameter()
            param.ParameterName = "@PregnancyValueOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtPregnancyOthers.Text.Trim() <> String.Empty, txtPregnancyOthers.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ReligionValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlReligion.SelectedValue = "-1", System.DBNull.Value, ddlReligion.SelectedValue)
            cmd.Parameters.Add(param)
            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ReligionValueOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtReligionOthers.Text.Trim() <> String.Empty, txtReligionOthers.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ColorValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlColor.SelectedValue = "-1", System.DBNull.Value, ddlColor.SelectedValue)
            cmd.Parameters.Add(param)
            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ColorValueOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtColorOthers.Text.Trim() <> String.Empty, txtColorOthers.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@AgeChecked"
            param.DbType = DbType.Int32
            param.Value = IIf(chkAge.Checked, 1, 0)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RetaliationValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlRetaliation.SelectedValue = "-1", System.DBNull.Value, ddlRetaliation.SelectedValue)
            cmd.Parameters.Add(param)
            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RetaliationOthers"
            param.DbType = DbType.String
            param.Value = IIf(txtRetaliationOther.Text.Trim() <> String.Empty, txtRetaliationOther.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DisabilityChecked"
            param.DbType = DbType.Int32
            param.Value = IIf(chkDisability.Checked, 1, 0)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DisabilityHistoryValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlDisabilityHistory.SelectedValue = "-1", System.DBNull.Value, ddlDisabilityHistory.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@WereMedicalRecordsSubmittedValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlMedicalRecords.SelectedValue = "-1", System.DBNull.Value, ddlMedicalRecords.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DidAskEmployerForAssistanceValueID"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlWorkingConditions.SelectedValue = "-1", System.DBNull.Value, ddlWorkingConditions.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RequestMadeToFirstName"
            param.DbType = DbType.String
            param.Value = IIf(txtReqToFName.Text.Trim() <> String.Empty, txtReqToFName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RequestMadeToLastName"
            param.DbType = DbType.String
            param.Value = IIf(txtReqToLName.Text.Trim() <> String.Empty, txtReqToLName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RequestTypeCode"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlHowRequestMade.SelectedValue = "-1", System.DBNull.Value, ddlHowRequestMade.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@RequestDescription"
            param.DbType = DbType.String
            param.Value = IIf(txtWorkCondionChanges.Text.Trim() <> String.Empty, txtWorkCondionChanges.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)

        Catch ex As Exception
            Console.Write(ex.Message)
        End Try
    End Sub

End Class