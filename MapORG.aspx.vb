﻿Imports WF.WatchForce.Data

Partial Public Class MapORG
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")

            If Not Page.IsPostBack Then
                'LoadORGs()
                'ddlBranches.Items.Add("Please Select")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    'Private Sub LoadORGs()
    '    Dim StrSQL As String

    '    Try

    '        StrSQL = "SELECT OrganizationFullName,OrganizationID, OrganizationAbbreviation " & _
    '                    "FROM( " & _
    '                    "SELECT '' OrganizationFullName, -1 OrganizationID, 'Please Select' OrganizationAbbreviation, 0 'SortOrder' " & _
    '                    "            UNION " & _
    '                    "SELECT OrganizationFullName, OrganizationID, OrganizationAbbreviation, 1 'SortOrder' FROM dbo.Organization" & _
    '                    ")TMP " & _
    '                    "ORDER BY TMP.SortOrder, TMP.OrganizationFullName"

    '        dtItems = New DataTable()
    '        If Not DBConn Is Nothing Then DBConn.getDataTable(StrSQL, dtItems)
    '        If Not dtItems Is Nothing Then
    '            ddlOrg.DataSource = dtItems
    '            ddlOrg.DataTextField = "OrganizationAbbreviation"
    '            ddlOrg.DataValueField = "OrganizationID"
    '            ddlOrg.DataBind()

    '            txtOrgName.Text = dtItems.Rows(1)("OrganizationFullName")
    '        End If

    '    Catch ex As Exception
    '        ObjError.LogError(ex)
    '    End Try

    'End Sub

    'Private Sub ddlOrg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrg.SelectedIndexChanged
    '    Try
    '        LoadBranchanges(ddlOrg.SelectedValue)
    '    Catch ex As Exception
    '        ObjError.LogError(ex)
    '    End Try
    'End Sub

    'Private Sub LoadBranchanges(ByVal OrgID As Integer)
    '    'Dim StrSQL As String

    '    Try
    '        'StrSQL = " SELECT OrganizationBranchID, OrganizationBranchName " & _
    '        '            "FROM( " & _
    '        '            "SELECT -1 OrganizationBranchID, 'Please Select' OrganizationBranchName, 0 'SortOrder' " & _
    '        '            " UNION " & _
    '        '            "SELECT OrganizationBranchID, OrganizationBranchName, 1 'SortOrder' FROM dbo.OrganizationBranch " & _
    '        '            "            WHERE(OrganizationID = " & OrgID & ")" & _
    '        '            ")TMP " & _
    '        '            "ORDER BY TMP.SortOrder, TMP.OrganizationBranchName"
    '        dtItems = New DataTable()
    '        'If Not DBConn Is Nothing Then DBConn.getDataTable(StrSQL, dtItems)

    '        If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(, dtItems)
    '        If Not dtItems Is Nothing Then
    '            ddlBranches.DataSource = dtItems
    '            ddlBranches.DataTextField = "OrganizationBranchName"
    '            ddlBranches.DataValueField = "OrganizationBranchID"
    '            ddlBranches.DataBind()
    '        End If

    '    Catch ex As Exception
    '        ObjError.LogError(ex)
    '    End Try
    'End Sub

    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Try
            Response.Redirect("ComplainantInfo.Aspx", False)
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try

    End Sub

    Private Sub btnValidateORG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidateORG.Click
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter

        Try
            
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "sp_Organization_Validate"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Input_OrganizationBranchCode"
            param.DbType = DbType.String
            param.Value = txtBranchCode.Text.Trim()

            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                txtOrgName.Text = dtItems.Rows(0)("OrganizationFullName")
                txtOrgDesc.Text = dtItems.Rows(0)("OrganizationDescription")
                txtBranch.Text = dtItems.Rows(0)("BranchName")
                txtBranchAddress.Text = dtItems.Rows(0)("BranchAddress")

                'lblValidationError.Text = ""
                lblValidationError.Visible = False
            Else
                lblValidationError.Visible = True
                'lblValidationError.Text = "Incorrect Branch Code ...!"

                txtOrgName.Text = String.Empty
                txtOrgDesc.Text = String.Empty
                txtBranch.Text = String.Empty
                txtBranchAddress.Text = String.Empty

            End If

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub
End Class