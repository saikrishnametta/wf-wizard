﻿Imports WF.WatchForce.Data
Imports System.Data.SqlClient

Partial Public Class ComplaintDetails
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql
    Dim ComplaintID, ContactID As Int64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            CalExtDtComplaintFiled.EndDate = DateTime.Today
            CalExtDtEmplmtEnd.EndDate = DateTime.Today
            CalExtHireDate.EndDate = DateTime.Today

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")
            manageVisibility()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProceed.Click
        Try
            saveComplaintDetails()

            If ddlDoUHaveAttorny.SelectedValue = "38" Then
                saveAttorney()
            End If

            'If ddlDoUHaveAttorny.SelectedValue = "38" Then
            '    Response.Redirect("AttorneyDetails.aspx", False)
            'Else
            '    Response.Redirect("DiscriminationDetails.aspx", False)
            'End If
            Response.Redirect("DiscriminationDetails.aspx", False)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub saveComplaintDetails()
        
        Dim cmd As SqlClient.SqlCommand
        Dim conn As SqlClient.SqlConnection
        Dim param As SqlClient.SqlParameter
        Dim provider

        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spAddComplaintDetails"
            cmd.CommandType = CommandType.StoredProcedure
            conn = New SqlClient.SqlConnection(DBConn.DBConnection)
            conn.Open()
            cmd.Connection = conn

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@BranchID"
            param.DbType = DbType.Int64
            param.Value = Session("BranchID")
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Tot#Emp"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlNoOfEmps.SelectedValue = "-1", System.DBNull.Value, ddlNoOfEmps.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DoesHaveAttorneyTypeCode"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlDoUHaveAttorny.SelectedValue = "-1", System.DBNull.Value, ddlDoUHaveAttorny.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@HasContactedAttorneyTypeCode"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlContactedAttorny.SelectedValue = "-1", System.DBNull.Value, ddlContactedAttorny.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintTypeCode"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlORGAssociation.SelectedValue = "-1", System.DBNull.Value, ddlORGAssociation.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@HireDateOrApplicationDate"
            param.DbType = DbType.Date
            provider = New System.Globalization.CultureInfo("en-US")
            'param.Value = IIf(IsDate(txtHireDate.Text.Trim()), Date.Parse(txtHireDate.Text.Trim(), provider), System.DBNull.Value)
            If IsDate(txtHireDate.Text.Trim()) Then param.Value = Date.Parse(txtHireDate.Text.Trim(), provider) Else param.Value = System.DBNull.Value
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@IsStillEmployed"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlStillEmployed.SelectedValue = "-1", System.DBNull.Value, ddlStillEmployed.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DtEmploymentEnd"
            param.DbType = DbType.DateTime
            provider = New System.Globalization.CultureInfo("en-US")
            'param.Value = IIf(IsDate(txtDtEmplmtEnd.Text.Trim()), Date.Parse(txtDtEmplmtEnd.Text.Trim(), provider), System.DBNull.Value)
            If IsDate(txtDtEmplmtEnd.Text.Trim()) Then param.Value = Date.Parse(txtDtEmplmtEnd.Text.Trim(), provider) Else param.Value = System.DBNull.Value
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@HowDidYourEmploymentEnd"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlHowEmplEnd.SelectedValue = "-1", System.DBNull.Value, ddlHowEmplEnd.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@HasComplaintFiledWithAgency"
            param.DbType = DbType.Int32
            param.Value = IIf(ddlFiledComplaint.SelectedValue = "-1", System.DBNull.Value, ddlFiledComplaint.SelectedValue)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@NameOfAgency"
            param.DbType = DbType.String
            param.Value = txtComplaintAgency.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintFiledDate"
            param.DbType = DbType.DateTime
            provider = New System.Globalization.CultureInfo("en-US")
            'param.Value = IIf(IsDate(txtDtComplaintFiled.Text.Trim()), Date.Parse(txtDtComplaintFiled.Text.Trim(), provider), System.DBNull.Value)
            If IsDate(txtDtComplaintFiled.Text.Trim()) Then param.Value = Date.Parse(txtDtComplaintFiled.Text.Trim(), provider) Else param.Value = System.DBNull.Value
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@JobTitle"
            param.DbType = DbType.String
            param.Value = txtJobTitle.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintShortDescription"
            param.DbType = DbType.String
            param.Value = txtComplaintTitle.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintLongDescription"
            param.DbType = DbType.String
            param.Value = txtComplaintDesr.Text.Trim()
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)
            'If Not cmd Is Nothing Then cmd.ExecuteNonQuery()

            If Not dtItems Is Nothing Then
                ComplaintID = dtItems.Rows(0)("ComplaintID")
                Session("ComplaintID") = ComplaintID
            End If

        Catch ex As Exception
            Dim i As Integer
        End Try
    End Sub

    Protected Sub saveAttorney()
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()

            cmd.CommandText = "spAddAttorneyContact"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintID"
            param.DbType = DbType.Int64
            param.Value = Session("ComplaintID")
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@FirstName"
            param.DbType = DbType.String
            param.Value = IIf(txtFName.Text.Trim() <> String.Empty, txtFName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@MiddleName"
            param.DbType = DbType.String
            param.Value = IIf(txtMName.Text.Trim() <> String.Empty, txtMName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@LastName"
            param.DbType = DbType.String
            param.Value = IIf(txtLName.Text.Trim() <> String.Empty, txtLName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Line1"
            param.DbType = DbType.String
            param.Value = IIf(txtAddress1.Text.Trim() <> String.Empty, txtAddress1.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Line2"
            param.DbType = DbType.String
            param.Value = IIf(txtAddress2.Text.Trim() <> String.Empty, txtAddress2.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Zip"
            param.DbType = DbType.Int32
            param.Value = IIf(txtSearchZip.Text.Trim() <> String.Empty, txtSearchZip.Text.Trim(), System.DBNull.Value) 'txtZip.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@HomePhone"
            param.DbType = DbType.String
            param.Value = IIf(txtHomePhone.Text.Trim() <> String.Empty, txtHomePhone.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@CellPhone"
            param.DbType = DbType.String
            param.Value = IIf(txtCellPhone.Text.Trim() <> String.Empty, txtCellPhone.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@WorkPhone"
            param.DbType = DbType.String
            param.Value = IIf(txtWorkPhone.Text.Trim() <> String.Empty, txtWorkPhone.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@PrimaryEmailID"
            param.DbType = DbType.String
            param.Value = IIf(txtPrimaryEmail.Text.Trim() <> String.Empty, txtPrimaryEmail.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@AlternateEmailID"
            param.DbType = DbType.String
            param.Value = IIf(txtAlternateEmail.Text.Trim() <> String.Empty, txtAlternateEmail.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)

            If Not dtItems Is Nothing Then
                ContactID = dtItems.Rows(0)("ContactID")
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub manageVisibility()
        Try
            If ddlDoUHaveAttorny.SelectedValue = "38" Then
                tblAtrnyContact.Visible = True
                ddlContactedAttorny.Enabled = True
            Else
                tblAtrnyContact.Visible = False
                ddlContactedAttorny.Enabled = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlDoUHaveAttorny_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDoUHaveAttorny.SelectedIndexChanged
        Try
            'manageVisibility()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtSearchZip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchZip.TextChanged
        Dim ZipAscs As String()
        Try

            If txtSearchZip.Text.Trim().Length > 0 AndAlso Not txtSearchZip.Text.Trim().Contains("Enter") Then
                ZipAscs = txtSearchZip.Text.Trim.Split("/")
            End If

            If Not ZipAscs Is Nothing AndAlso ZipAscs.Count > 2 Then
                If rdgZip.SelectedValue = "1" Then
                    txtSearchZip.Text = ZipAscs(1).Trim()
                Else
                    txtSearchZip.Text = ZipAscs(0).Trim()
                End If
                txtZipResult.Text = ZipAscs(0).Trim()
                txtCity.Text = ZipAscs(1).Trim()
                txtState.Text = ZipAscs(2).Trim()
            End If

        Catch ex As Exception

        End Try
    End Sub

    <System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()> Public Shared Function SearchZip(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim strSql As String
        Dim objCmd As SqlClient.SqlCommand
        Dim dtItems As DataTable
        Try


            strSql = "SELECT " & _
                     "ISNULL(CONVERT(VARCHAR,ZIP),'') + ' / ' + " & _
                     "ISNULL(CONVERT(VARCHAR,Primary_City),'') + ' / ' + " & _
                     "ISNULL(CONVERT(VARCHAR,State),'') ZipFull " & _
                                "FROM DBO.ZIPCODE " & _
                     "WHERE ZIP LIKE @ZIP " & _
                     "ORDER BY ZIP"

            objCmd = New SqlClient.SqlCommand()
            objCmd.Connection = New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("WFConnString").ConnectionString)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = strSql
            objCmd.Parameters.AddWithValue("@ZIP", prefixText + "%")

            Dim objAdapter As New SqlDataAdapter()
            dtItems = New DataTable()

            objAdapter.SelectCommand = objCmd
            'objAdapter.SelectCommand.Connection = mobjConnection
            objAdapter.SelectCommand.CommandTimeout = 0
            objAdapter.Fill(dtItems)
            objAdapter.Dispose()
            objAdapter = Nothing

            Dim Zips As List(Of String) = New List(Of String)
            For Each dtrow In dtItems.Rows
                Zips.Add(dtrow("ZipFull").ToString)
            Next

            Return Zips

        Catch ex As Exception
            Console.Write(ex.Message)
            'ObjError.LogError(ex)
        End Try

    End Function

End Class