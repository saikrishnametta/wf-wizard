﻿Imports WF.WatchForce.Data
Imports System.Data.SqlClient

Partial Public Class IncidentDetails
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql
    Dim ComplaintID, IncidentID As Int64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")

            CalendarExtender1.EndDate = DateTime.Today

            If Not Page.IsPostBack Then
                LoadValues()
                ViewState("Mode") = "Add"
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub LoadValues()
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter

        Try

            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spGetIncidents"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintID"
            param.DbType = DbType.Int64
            param.Value = Session("ComplaintID")

            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                gvwIncidents.DataSource = dtItems.DefaultView
                gvwIncidents.DataBind()

                dtItems.PrimaryKey = New DataColumn() {dtItems.Columns("IncidentID")}

                ViewState("dtIncidents") = dtItems
            Else
                gvwIncidents.EmptyDataText = "There are no incidents .."
            End If

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Try
            Response.Redirect("WitnessDetails.aspx", False)
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub gvwIncidents_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvwIncidents.RowDeleting
        Dim IncidentID As Int64
        Try
            'ContactId = gvwHarassContacts.Rows(gvwHarassContacts.EditIndex).FindControl("ContactID").
            If gvwIncidents.DataKeys.Count > 0 Then
                IncidentID = gvwIncidents.DataKeys(e.RowIndex)(0).ToString()
                DeleteIncident(IncidentID)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub DeleteIncident(ByVal IncidentID As Int64)
        Try
            DBConn.ExecuteStatement("UPDATE dbo.Incident SET Row_Delete = 1 WHERE IncidentID = " & IncidentID)
            LoadValues()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub gvwIncidents_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvwIncidents.RowEditing
        Try
            'ContactId = gvwHarassContacts.Rows(gvwHarassContacts.EditIndex).FindControl("ContactID").
            If gvwIncidents.DataKeys.Count > 0 Then
                IncidentID = gvwIncidents.DataKeys(e.NewEditIndex)(0).ToString()
                pickIncident(IncidentID)
                ViewState("Mode") = "Update"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub pickIncident(ByVal IncidentID As Int64)
        Dim drIncident As DataRow
        Try
            dtItems = ViewState("dtIncidents")
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                drIncident = dtItems.Rows.Find(New Object() {IncidentID})
                If Not drIncident Is Nothing Then
                    hdnIncidentID.Value = drIncident("IncidentID")
                    txtDtIncident.Text = drIncident("DateOfIncident")
                    txtIncidentTitle.Text = drIncident("IncidentTitle")
                    txtIncidentDesr.Text = drIncident("IncidentDescription")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub clearControls()
        Try
            hdnIncidentID.Value = ""
            txtDtIncident.Text = ""
            txtIncidentTitle.Text = ""
            txtIncidentDesr.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnAddIncident_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddIncident.Click
        Try
            clearControls()
            ViewState("Mode") = "Add"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter
        Dim provider
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()

            If ViewState("Mode") = "Add" Then
                cmd.CommandText = "spAddIncident"
            ElseIf ViewState("Mode") = "Update" Then
                cmd.CommandText = "spUpdateIncident"
            End If

            cmd.CommandType = CommandType.StoredProcedure

            If ViewState("Mode") = "Add" Then
                param = New SqlClient.SqlParameter()
                param.ParameterName = "@ComplaintID"
                param.DbType = DbType.Int64
                param.Value = Session("ComplaintID")
                cmd.Parameters.Add(param)
            ElseIf ViewState("Mode") = "Update" Then
                param = New SqlClient.SqlParameter()
                param.ParameterName = "@IncidentID"
                param.DbType = DbType.Int64
                param.Value = hdnIncidentID.Value
                cmd.Parameters.Add(param)
            End If

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DateOfIncident"
            param.DbType = DbType.Date
            provider = New System.Globalization.CultureInfo("en-US")
            'param.Value = IIf(txtDtIncident.Text.Trim() <> String.Empty, txtDtIncident.Text.Trim(), System.DBNull.Value)
            'If IsDate(txtDtIncident.Text.Trim()) Then param.Value = Date.Parse(txtDtIncident.Text.Trim(), provider) Else param.Value = System.DBNull.Value
            If txtDtIncident.Text.Trim() <> String.Empty Then param.Value = Date.Parse(txtDtIncident.Text.Trim(), provider) Else param.Value = System.DBNull.Value
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@IncidentTitle"
            param.DbType = DbType.String
            param.Value = IIf(txtIncidentTitle.Text.Trim() <> String.Empty, txtIncidentTitle.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@IncidentDescription"
            param.DbType = DbType.String
            param.Value = IIf(txtIncidentDesr.Text.Trim() <> String.Empty, txtIncidentDesr.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)

            If Not dtItems Is Nothing Then
                IncidentID = dtItems.Rows(0)("IncidentID")
                LoadValues()
                clearControls()
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class