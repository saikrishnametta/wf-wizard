﻿Imports WF.WatchForce.Data

Partial Public Class SelectORG
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'If (Session("UserID") = Nothing) Then
            '    Server.Transfer("TimeOut.aspx", False)
            'End If

            'Dim connString = ConfigurationManager.ConnectionStrings("WFConnString").ConnectionString

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")


            'If Not DBConn Is Nothing Then
            '    lblORGName.Text = "Dbconn Exists "
            '    lblORGName.Text &= DBConn.DBConnection()
            'Else
            '    lblORGName.Text = "Dbconn failes"
            'End If

            If Not Page.IsPostBack Then
                'LoadORGs()
                'ddlBranches.Items.Add("Please Select")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Try
            'Response.Redirect("PersonalDetails.aspx", False)
            Response.Redirect("ComplaintDetails.aspx", False)
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try

    End Sub

    Private Sub btnValidateORG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidateORG.Click
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter

        Try

            'chkAgreeRules.Checked = False
            'btnProceed.Enabled = False

            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "sp_Organization_Validate"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Input_OrganizationBranchCode"
            param.DbType = DbType.String
            param.Value = txtBranchCode.Text.Trim()

            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                lblORGName.Text = dtItems.Rows(0)("OrganizationFullName")
                lblORGDesc.Text = dtItems.Rows(0)("OrganizationDescription")
                lblBranchName.Text = dtItems.Rows(0)("BranchName")
                lblBranchAddr.Text = dtItems.Rows(0)("BranchAddress")

                Session("BranchID") = dtItems.Rows(0)("BranchID")

                'lblValidationError.Text = ""
                lblValidationError.Visible = False
            Else
                lblValidationError.Visible = True
                'lblValidationError.Text = "Incorrect Branch Code ...!"

                lblORGName.Text = String.Empty
                lblORGDesc.Text = String.Empty
                lblBranchName.Text = String.Empty
                lblBranchAddr.Text = String.Empty

            End If

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

End Class