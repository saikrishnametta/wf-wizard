﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="LogIn.aspx.vb" Inherits="WF.LogIn" 
    title="Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder"  runat="server">

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager> 

<script type="text/javascript" id="validate" language="javascript">

   


</script>

<table width="100%">
<tr>
<td style="width:5%">
</td>
<td style="width:90%">

    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="100%">
     <tr>
        <td align="center">            
            <asp:Label ID="lblHeader" Font-Bold="true" Font-Size="18px" runat="server" Text="Login"></asp:Label>
        </td>
     </tr>
     <tr>
        <td align="center">            
            &nbsp;</td>
     </tr>
     <tr>
        <td align="center">            
            &nbsp;</td>
     </tr>
    </table>
    
<table width="100%">
<tr>
<td style="width:65%" align="left">

    <p style="language:en-IN;margin-top:0pt;margin-bottom:0pt;margin-left:0in;
text-align:left;direction:ltr;unicode-bidi:embed;mso-line-break-override:
none;word-break:normal;punctuation-wrap:hanging" align="left">
        <u style="text-underline:single">
        <span style="font-size:10.0pt;font-family:Helvetica;mso-ascii-font-family:Helvetica;
mso-fareast-font-family:Verdana;mso-bidi-font-family:Helvetica;color:#7F7F7F;
mso-color-index:1;mso-font-kerning:12.0pt;language:en-US;font-weight:bold">Is this your 
        first time here ?</span></u></p>

</td>
<td style="width:35%">

    <p style="language:en-IN;margin-top:0pt;margin-bottom:0pt;margin-left:0in;
text-align:center;direction:ltr;unicode-bidi:embed;mso-line-break-override:
none;word-break:normal;punctuation-wrap:hanging">
        <u style="text-underline:single">
        <span style="font-size:10.0pt;font-family:Helvetica;mso-ascii-font-family:Helvetica;
mso-fareast-font-family:Verdana;mso-bidi-font-family:Helvetica;color:#7F7F7F;
mso-color-index:1;mso-font-kerning:12.0pt;language:en-US;font-weight:bold">Returning to 
        the WatchForce website ? </span></u>
    </p>

</td>
</tr>
<tr>
<td style="width:65%" align="left">

    &nbsp;</td>
<td style="width:35%">

    &nbsp;</td>
</tr>
<tr>
<td style="width:65%" valign="top">
    <br />
    1. Fill out the New Account form with your details. <br />
    2. An email will be immediately sent to your email address.<br />
    3. Read your email, and click on the web link it contains.<br />
    4. Your account will be confirmed and you will be logged in.

    <br />
    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
    <br />
    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <asp:Button ID="btnCreateNewAcc" CssClass="btnstyle" runat="server" 
        Text="Create new account" Width="150px" 
                            Height="28px" />
          <cc1:RoundedCornersExtender ID="btnCreateNewAcc_RoundedCornersExtender" 
        TargetControlID="btnCreateNewAcc" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>

</td>
<td style="width:35%">

<table border="0" style="background-color:#f1f1f1" width="100%">
    <tr>
        
        
        <td style="width:100%" valign="top">
            <table border="0" width="100%">
                <tr>
                    <td class="lblContentV2" align="left">
                        <p style="language:en-IN;margin-top:0pt;margin-bottom:0pt;margin-left:0in;
text-align:left;direction:ltr;unicode-bidi:embed;mso-line-break-override:none;
word-break:normal;punctuation-wrap:hanging">
                            <span style="font-size:14.0pt;font-family:Helvetica;
mso-ascii-font-family:Helvetica;mso-fareast-font-family:+mn-ea;mso-bidi-font-family:
Helvetica;color:black;mso-color-index:1;mso-font-kerning:12.0pt;language:en-US">&nbsp;&nbsp;&nbsp; Sign In</span></p>
                        <p style="language:en-IN;margin-top:0pt;margin-bottom:0pt;margin-left:0in;
text-align:left;direction:ltr;unicode-bidi:embed;mso-line-break-override:none;
word-break:normal;punctuation-wrap:hanging">
                            &nbsp;</p>
                        <p style="language:en-IN;margin-top:0pt;margin-bottom:0pt;margin-left:0in;
text-align:left;direction:ltr;unicode-bidi:embed;mso-line-break-override:none;
word-break:normal;punctuation-wrap:hanging">
                            <span style="font-size:10.0pt;
font-family:Helvetica;mso-ascii-font-family:Helvetica;mso-fareast-font-family:
+mn-ea;mso-bidi-font-family:Helvetica;color:black;mso-color-index:1;mso-font-kerning:
12.0pt;language:en-US;font-weight:bold">&nbsp;&nbsp;&nbsp;&nbsp; Username</span><span style="font-size:
10.0pt;font-family:Helvetica;mso-ascii-font-family:Helvetica;mso-fareast-font-family:
+mn-ea;mso-bidi-font-family:Helvetica;color:black;mso-color-index:1;mso-font-kerning:
12.0pt;language:en-US;font-weight:bold"> </span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtUserID" runat="server" MaxLength="30" Width="80%"></asp:TextBox>                            
                    </td>
                </tr>
                <tr>
                    <td class="lblContentV2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="lblContentV2">
                        <p style="language:en-IN;margin-top:0pt;margin-bottom:0pt;margin-left:0in;
text-align:left;direction:ltr;unicode-bidi:embed;mso-line-break-override:none;
word-break:normal;punctuation-wrap:hanging">
                            <span style="font-size:10.0pt;
font-family:Helvetica;mso-ascii-font-family:Helvetica;mso-fareast-font-family:
+mn-ea;mso-bidi-font-family:Helvetica;color:black;mso-color-index:1;mso-font-kerning:
12.0pt;language:en-US;font-weight:bold">&nbsp;&nbsp;&nbsp; Password</span><span style="font-size:
10.0pt;font-family:Helvetica;mso-ascii-font-family:Helvetica;mso-fareast-font-family:
+mn-ea;mso-bidi-font-family:Helvetica;color:black;mso-color-index:1;mso-font-kerning:
12.0pt;language:en-US;font-weight:bold"> </span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtPassCode" runat="server" MaxLength="30" TextMode="Password" Width="80%"></asp:TextBox>                            
                        <br />
                        <br />
&nbsp;&nbsp;&nbsp;&nbsp;
          <asp:Button ID="btnSignIn" CssClass="btnstyle" runat="server" Text="Sign In" Width="100px" 
                            Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" TargetControlID="btnSignIn" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
                        <br />
                        <br />
                        <span style="font-size:10.0pt;font-family:Helvetica;
mso-ascii-font-family:Helvetica;mso-fareast-font-family:+mn-ea;mso-bidi-font-family:
Helvetica;color:#0070C0;mso-font-kerning:12.0pt;language:en-US">&nbsp;&nbsp;&nbsp; Can’t access your account ?</span><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                </table>
        </td>
    </tr>
    </table>

</td>
</tr>
<tr>
<td style="width:65%" valign="top">
    &nbsp;</td>
<td style="width:35%">

    &nbsp;</td>
</tr>
</table>
    
        
    
    
    
    
    <table border = "0" width="100%">  
      <tr><td>&nbsp;</td></tr>
      </table>  
      
 </td>
<td style="width:5%">
</td>
</tr>
</table>

</asp:Content>