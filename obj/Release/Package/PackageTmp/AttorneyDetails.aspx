﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="AttorneyDetails.aspx.vb" Inherits="WF.AttorneyDetails" 
    title="Attorney Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder"  runat="server">

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager> 

<script type="text/javascript" id="validate" language="javascript">

    
    function ValidatePhone(sender, args) {
        var txt1 = document.getElementById("<%= txtHomePhone.ClientID %>");
        var txt2 = document.getElementById("<%= txtWorkPhone.ClientID%>");
        var txt3 = document.getElementById("<%= txtCellPhone.ClientID%>");
        //alert(txt1.value);
        args.IsValid = (txt1.value != "") || (txt2.value != "") || (txt3.value != "");
    }
       
</script>

    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="90%">
     <tr>
        <td>            
            <asp:Label ID="lblHeader" Font-Bold="true" Font-Size="Medium" runat="server" CssClass="accordionHeader" Text="Attorney Details"></asp:Label>
        </td>
     </tr>
    </table>
    
    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="90%">
      
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td>
        
        <div id="countrydivcontainer" style="border:1px solid gray; width:95%; margin-bottom: 1em; padding: 10px">        
        <table border="0" width="100%">   
        <tr><td colspan="2"> &nbsp;</td> </tr>                    
        <tr>
            <td style="width:55%" valign="top">
                <table width="100%" border= "0">
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            First Name *
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtFName" MaxLength="100" runat="server" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="txtFName" WatermarkCssClass="watermarked"
                                WatermarkText="Mandatory .." runat="server" ></cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFName" FilterType="UppercaseLetters, LowercaseLetters" >
                            </cc1:FilteredTextBoxExtender>                                                   
                            <asp:RequiredFieldValidator ID="rfvtxtFName" runat="server" ControlToValidate="txtFName" InitialValue="" ErrorMessage="Please enter first name ..!">
                            &nbsp; <asp:Image ID="ImgErrFname" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter first name ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Middle Name
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtMName" runat="server" MaxLength="100" ></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtMName" FilterType="UppercaseLetters, LowercaseLetters" >
                            </cc1:FilteredTextBoxExtender>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Last Name *
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtLName" runat="server" MaxLength="100" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" TargetControlID="txtLName" WatermarkCssClass="watermarked"
                                WatermarkText="Mandatory .." runat="server" ></cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtLName" FilterType="UppercaseLetters, LowercaseLetters" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvtxtLName" runat="server" ControlToValidate="txtLName" InitialValue="" ErrorMessage="Please enter last name ..!">
                            &nbsp; <asp:Image ID="ImgErrLName" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter last name ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>                  
                        </td> 
                    </tr>
                                        
                </table>
            </td>
            <td style="width:2%"></td>
            <td style="width:43%">
                <table width="100%" border= "0">
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Address Line *
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtAddress1" runat="server" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtAddress1" WatermarkCssClass="watermarked"
                                WatermarkText="Mandatory .." runat="server" ></cc1:TextBoxWatermarkExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAddress1" InitialValue="" ErrorMessage="Please fill address..!">
                            &nbsp; <asp:Image ID="Image1" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please fill address ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Address Line 2
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtAddress2" runat="server" ></asp:TextBox>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            ZIP *
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtZip" runat="server" AutoPostBack="true" OnTextChanged="zipSelected"  MaxLength="6"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtZip" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                            <%--<cc1:AutoCompleteExtender
                                runat="server" 
                                BehaviorID="AutoCompleteEx"
                                ID="autoComplete1" 
                                TargetControlID="txtZip"
                                ServicePath="AutoPopoulate.asmx" 
                                ServiceMethod="HelloWorld"
                                MinimumPrefixLength="3" 
                                CompletionInterval="1000"
                                EnableCaching="true"
                                CompletionSetCount="20"                                
                                DelimiterCharacters=";, :"
                                ShowOnlyCurrentWordInCompletionListItem="true" >                                
                            </cc1:AutoCompleteExtender>    --%>   
                            <cc1:AutoCompleteExtender ServiceMethod="SearchZip"
                                MinimumPrefixLength="3"
                                CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" 
                                TargetControlID="txtZip"
                                ID="AutoCompleteExtender1" runat="server" FirstRowSelected = "false">
                            </cc1:AutoCompleteExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtZip" InitialValue="" ErrorMessage="Please enter Zip..!">
                            &nbsp; <asp:Image ID="Image6" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter Zip ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            City
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:Label ID="lblCity" Text="-" class="lblContent" runat="server"></asp:Label>                            
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            State
                        </td>
                        <td style="width:60%" valign="top">                            
                            <asp:Label ID="lblState" Text="-" class="lblContent" runat="server"></asp:Label>
                        </td> 
                     </tr>
                     <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            ZIP
                        </td>
                        <td style="width:60%" valign="top">                            
                            <asp:Label ID="lblZip" Text="-" class="lblContent" runat="server"></asp:Label>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Home Phone
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtHomePhone" runat="server" MaxLength="12"></asp:TextBox>
                            <%--<cc1:TextBoxWatermarkExtender ID="WatermarkExtender1" TargetControlID="txtHomePhone" WatermarkCssClass="watermarked"
                                WatermarkText="Atleast 1 phone mandatory" runat="server" ></cc1:TextBoxWatermarkExtender>--%>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtHomePhone" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:CustomValidator ID="cvPhone" runat="server" Display="Dynamic" ErrorMessage="Atleast 1 phone mandatory" ClientValidationFunction="ValidatePhone"
                                ValidateEmptyText="true">
                              &nbsp; <asp:Image ID="Image5" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Atleast 1 phone mandatory ..!" ImageUrl = "./images/err.jpg" />
                                </asp:CustomValidator>

                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Work Phone
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtWorkPhone" runat="server" MaxLength="12"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtWorkPhone" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Cell Phone
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="12"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtCellPhone" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Primary Email ID *
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtPrimaryEmail" runat="server" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" TargetControlID="txtPrimaryEmail" WatermarkCssClass="watermarked"
                                WatermarkText="Mandatory .." runat="server" ></cc1:TextBoxWatermarkExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPrimaryEmail" InitialValue="" ErrorMessage="Please enter email ..!">
                            &nbsp; <asp:Image ID="Image2" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter email ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmail1" runat="server" Display="Dynamic"  ControlToValidate="txtPrimaryEmail" Text= "" ErrorMessage="" 
                            ValidationExpression ="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >
                            <asp:Image ID="Image3" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Invalid Email ..!" ImageUrl = "./images/err.jpg" />                           
                            </asp:RegularExpressionValidator>
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:40%" class="lblContent">
                            Alternate Email ID
                        </td>
                        <td style="width:60%" valign="top">
                            <asp:TextBox ID="txtAlternateEmail" runat="server" ></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"  ControlToValidate="txtAlternateEmail" Text= "" ErrorMessage="" 
                            ValidationExpression ="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >
                            <asp:Image ID="Image4" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Invalid Email ..!" ImageUrl = "./images/err.jpg" />                           
                            </asp:RegularExpressionValidator>
                        </td> 
                    </tr>
                </table>
            </td>
        </tr>
      </table>
        </div>
          
        </td>
      </tr>
     
    </table>  
    
    
    
    <table border = "0" width="90%">  
      <%--<tr>
        <td colspan="2"><br /></td>
      </tr> --%>     
      <tr>      
      <td align="right" style="width:92%">
          <asp:Button ID="btnProceed" CssClass="btnstyle" runat="server" Text="Save & Continue" Width="140px" Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender2" TargetControlID="btnProceed" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
          </td>
      <td style="width:8%"></td>
      </tr>
      <tr><td colspan="2">&nbsp;</td></tr>
      </table>  
      
</asp:Content>