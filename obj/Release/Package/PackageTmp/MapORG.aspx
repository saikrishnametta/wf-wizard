﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MapORG.aspx.vb" Inherits="WF.MapORG" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>SELECT ORGANIZATION</title>
    <link href="fontStyles.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            height: 246px;
        }
        .style2
        {
            width: 210px;
        }
    </style>
</head>
<body class="body">
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <table border = "0" cellpadding="20" width="90%">
        <tr>
            <td align="center">
            
                    <table border="0" cellpadding="4" cellspacing="0"  align="left" width="90%"> 
                   <tr>
                     <td>
                         <table class="PageMainTable" border="0" >
                            <%--<tr>
                                <td class="PageHeader" align="center" valign="middle" rowspan="2" >
                                         <asp:Label ID="Label2" runat="server" ForeColor="white" Font-Bold="true"
                                               Text="WELCOME NETWORK INCIDENT REPORT WIZARD">
                                    </asp:Label>
                                </td>
                            </tr>
                     
                            <tr>
                                <td valign="middle">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td valign="middle" align="center">
                                    <asp:Label ID="Label10" runat="server" ForeColor="white" Font-Bold="false"
                                               Text="ORG Mapping ..">
                                    </asp:Label>
                                </td>
                            </tr>--%>
                            <tr>
                                <td style="width:15%" align="left">
                                <img src="./images/watchforce-system-logo.png" alt="WatchForce System - Accountability" style="height:110px" border="0"/>
                                </td>
                                <td style="width:85%" align="center">
                                    <asp:Label ID="Label2" runat="server" Font-Size="X-Large" ForeColor="White"
                                               Text="WATCHFORCE INCIDENT REPORT WIZARD"></asp:Label>
                                </td>
                            </tr>
                         </table>
                     </td>
                   </tr>
               <tr>
               <td class="style1">
              <table border="0" width="100%">   
                <tr><td colspan="3"> &nbsp;</td> </tr>                    
                <tr>
                 <td>&nbsp;</td> 
                  <td class="normal" style="width:25%">
                    &nbsp;<asp:Label runat="server" ID="lblBranchCode" Text="Enter Branch Code : "></asp:Label></td>
                  
                  <td style="width: 75%" align="left">
                   <asp:TextBox runat="server" ID="txtBranchCode"></asp:TextBox>
                    <%--<asp:DropDownList ID="ddlOrg" runat="server" AutoPostBack="true" ></asp:DropDownList>--%>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnValidateORG" CssClass="btnstyle" runat="server" Text="Validate" />
                  </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="left">
                        <asp:Label ID="lblValidationError" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr><td colspan="3"> &nbsp;</td> </tr>
              </table>
              <table border="1" style="border-color:Silver" cellpadding="8" cellspacing="0"  align="center" width="90%">
                  <tr>
                    <td>
                        <table border="0" width="100%">                       
                          <tr>
                            <td align="left">
                                Org Name
                            </td>
                            <td>
                                <asp:TextBox Width="70%" ID="txtOrgName" runat="server"></asp:TextBox>                                
                            </td>
                            <td>
                                Org Description
                            </td>
                            <td>
                                <asp:TextBox Width="90%" ID="txtOrgDesc" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                          </tr>
                          <tr>
                            <td align="left">
                                Branch Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtBranch" Width="70%" runat="server"></asp:TextBox>
                                <%--<asp:DropDownList Width="70%" ID="ddlBranches" runat="server" ></asp:DropDownList>--%>
                            </td>
                            <td>
                                Branch Address
                            </td>
                            <td>
                                <asp:TextBox ID="txtBranchAddress" Width="90%" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                          </tr>
                      </table>
                    </td>
                  </tr>
              </table>              
              
              <table border = "0" width="95%">
              <tr><td>&nbsp;</td></tr>
              <tr><td align="right" class="style2">
                  <asp:Button ID="btnProceed" CssClass="btnstyle" runat="server" Text="Proceed" Width="81px" />
                  </td></tr></table>
        </td>
        </tr>
        
        </table>
            </td>
        </tr>
     </table>
     
     
    </form>
</body>
</html>
