﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="WF._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="fontStyles.css" type="text/css" rel="stylesheet" />
</head>
<body class="body">
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <table border = "0" cellpadding="20" width="90%">
        <tr>
            <td align="center">
            
                    <table border="2" cellpadding="4" cellspacing="0"  align="left" width="90%"> 
                   <tr>
                     <td>
                         <table class="PageMainTable" border="0" >
                            <tr>
                                <td class="PageHeader" align="center" valign="middle" rowspan="2" >
                                         <asp:Label ID="Label2" runat="server" ForeColor="white" Font-Bold="true"
                                               Text="WELCOME NETWORK INCIDENT REPORT WIZARD">
                                    </asp:Label>
                                </td>
                            </tr>
                     
                            <tr>
                                <td valign="middle">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td valign="middle" align="center">
                                    <asp:Label ID="Label10" runat="server" ForeColor="white" Font-Bold="false"
                                               Text="ORG Mapping ..">
                                    </asp:Label>
                                </td>
                            </tr>
                         </table>
                     </td>
                   </tr>
               <tr>
               <td>
              <table border="0" width="100%">   
                <tr><td colspan="3"> &nbsp;</td> </tr>                    
                <tr>
                 <td>&nbsp;</td> 
                  <td class="normal" style="width:25%">
                    &nbsp;<asp:Label runat="server" ID="lblORGCode" Text="Enter ORG Code : "></asp:Label>
                  </td>
                  
                  <td style="width: 75%">
                    <asp:TextBox runat="server" ID="txtORGCode"></asp:TextBox>                                          
                  </td>
                </tr>
                <tr><td colspan="3"> &nbsp;</td> </tr>
              </table>
              <table border="1" cellpadding="6" cellspacing="0"  align="center" width="90%">
                  <tr>
                    <td>
                        <table border="0" width="100%">                       
                          <tr>
                            <td>
                                Org Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Org Description
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrgDesc" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                          </tr>
                          <tr>
                            <td>
                                Branch Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtBranch" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Branch Address
                            </td>
                            <td>
                                <asp:TextBox ID="txtBranchAddress" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                          </tr>
                      </table>
                    </td>
                  </tr>
              </table>
              <table><tr><td>&nbsp;</td></tr></table>
              
        </td>
        </tr>
        
        </table>
            </td>
        </tr>
     </table>
     
     
    </form>
</body>
</html>
