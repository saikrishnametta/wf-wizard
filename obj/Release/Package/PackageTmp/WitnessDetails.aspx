﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="WitnessDetails.aspx.vb" Inherits="WF.WitnessDetails" 
    title="Witness Details" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder"  runat="server">

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager> 

<script type="text/javascript" id="validate" language="javascript">

    function populateZipDeps() {

        //alert(document.getElementById('<%=txtSearchZip.ClientID%>').value);       

    }


</script>

<table width="100%">
<tr>
<td style="width:5%">
</td>
<td style="width:90%">


    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="100%">
     <tr>
        <td align="center">            
            <asp:Label ID="lblHeader" Font-Bold="true" Font-Size="18px" runat="server" Text="Witness Details"></asp:Label>
        </td>
     </tr>
    </table>
    
    
    <table border="0" width="100%">
        <tr>
            <td colspan="3" class="lblContentV2">
                Please identify the individuals who Witnessed            
            </td>
        </tr>            
        <tr>
            <td style="width:10%"></td>
            <td align="right" style="width:50%">                
                <asp:GridView ID="gvwHarassContacts" runat="server" BorderStyle="Solid" BorderWidth="1"                                 
                                  EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-VerticalAlign="Middle"                               
                                  Width="80%"  CellPadding="4" EmptyDataText="No Contacts Added .." 
                                  GridLines="Vertical" AutoGenerateColumns="False" DataKeyNames="ContactID" 
                                  CssClass="gridview" AlternatingRowStyle-CssClass="even" 
                                  AllowSorting="False"  AllowPaging="True" PageSize="5">
                       <AlternatingRowStyle CssClass="even" />
                       <Columns>
                            <asp:BoundField  DataField="ContactID" HeaderText="ContactID" SortExpression="ContactID" Visible="False"/> 
                             <asp:TemplateField HeaderText="Serial No"  >
                                <ItemTemplate>    
                                   <%# CType(Container, GridViewRow).RowIndex + 1%>
                                </ItemTemplate>
                             </asp:TemplateField> 
                            <asp:TemplateField HeaderText="First Name" SortExpression="FirstName">                                
                                <ItemTemplate>
                                    <asp:Label ID="gvLblFirstName" runat="server" Text='<%# Bind("FirstName") %>' ></asp:Label>
                                </ItemTemplate>
                           </asp:TemplateField> 
                           <asp:TemplateField HeaderText="Middle Name" SortExpression="MiddleName">                                
                                <ItemTemplate>
                                    <asp:Label ID="gvLblMiddleName" runat="server" Text='<%# Bind("MiddleName") %>' ></asp:Label>
                                </ItemTemplate>
                           </asp:TemplateField> 
                           <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">                                
                                <ItemTemplate>
                                    <asp:Label ID="gvLblLastName" runat="server" Text='<%# Bind("LastName") %>' ></asp:Label>
                                </ItemTemplate>
                           </asp:TemplateField>
		                   <asp:TemplateField >
                                    <ItemTemplate>
                                        <asp:LinkButton CommandName="Edit" CausesValidation="True" ValidationGroup="Test" ID="btnEdit" runat="server" Text="Edit"></asp:LinkButton>
                                    </ItemTemplate>                                    
                                    <ItemStyle HorizontalAlign="Left" />
                           </asp:TemplateField>                                    
                           <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete" 
                                    Runat="server"                                     
                                    OnClientClick="return confirm('Are you sure you want to delete this contact?');" 
                                    CommandName="Delete" CausesValidation="False">Delete</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="Left" />                                
                           </asp:TemplateField>                                                  
                        </Columns>
                    </asp:GridView>
            </td>
            <td style="width:40%"></td>
        </tr>           
        <tr>
            <td style="width:10%"></td>
            <td align="right" style="width:50%">
                <asp:ImageButton ID="btnAddContact" src="./images/AddHarass.png" Width="200px" Height="40px" runat="server" CausesValidation="false" />  
            </td>
            <td style="width:40%"></td>
     </tr>
     <tr>
        <td colspan="3">&nbsp;</td>
     </tr>
    </table>
    
    <table border="0" style="background-color:#f1f1f1" width="100%">
    <tr>
        <td style="width:60%">
            <table border="0" width="100%">
                <tr>
                    <td colspan="3">
                        <asp:HiddenField ID="hdnContactID" runat="server" />
                    </td>
                 </tr>
                <tr>
                    <td colspan="3" class="lblContentV2">
                        Name
                    </td>
                </tr>
                <tr>
                    <td style="width:35%" valign="middle">
                        <asp:TextBox ID="txtFName" MaxLength="100" runat="server" Width="78%" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="txtFName" WatermarkCssClass="watermarked"
                                WatermarkText=" First" runat="server" ></cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFName" FilterType="UppercaseLetters, LowercaseLetters" >
                            </cc1:FilteredTextBoxExtender>                                                   
                            <asp:RequiredFieldValidator ID="rfvtxtFName" runat="server" ControlToValidate="txtFName" InitialValue="" ErrorMessage="Please enter first name ..!">
                            &nbsp; <asp:Image ID="ImgErrFname" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter first name ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                    </td>
                    <td style="width:30%" valign="middle">
                        <asp:TextBox ID="txtMName" runat="server" MaxLength="100" ></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtMName" FilterType="UppercaseLetters, LowercaseLetters" >
                            </cc1:FilteredTextBoxExtender>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" TargetControlID="txtMName" WatermarkCssClass="watermarked"
                                WatermarkText=" Middle" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td style="width:35%" valign="middle">
                        <asp:TextBox ID="txtLName" runat="server" MaxLength="100" Width="78%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" TargetControlID="txtLName" WatermarkCssClass="watermarked"
                                WatermarkText=" Last" runat="server" ></cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtLName" FilterType="UppercaseLetters, LowercaseLetters" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvtxtLName" runat="server" ControlToValidate="txtLName" InitialValue="" ErrorMessage="Please enter last name ..!">
                            &nbsp; <asp:Image ID="ImgErrLName" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter last name ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="lblContentV2">
                        Job Title
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtJobTitle" Width="50%" runat="server" MaxLength="100" ></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td colspan="3" class="lblContentV2">
                        Address
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtAddress1" runat="server" Width="92%" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtAddress1" WatermarkCssClass="watermarked"
                                WatermarkText="                Line1" runat="server" ></cc1:TextBoxWatermarkExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAddress1" InitialValue="" ErrorMessage="Please fill address..!">
                            &nbsp; <asp:Image ID="Image1" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please fill address ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtAddress2" runat="server" Width="92%"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" TargetControlID="txtAddress2" WatermarkCssClass="watermarked"
                                WatermarkText="                Line2" runat="server" ></cc1:TextBoxWatermarkExtender>
                    </td>                    
                </tr>
                <tr>
                    <td colspan="3" class="lblContentV2">
                        Search for City
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="lblContentV2">
                      <asp:RadioButtonList ID="rdgZip" repeatdirection="Horizontal" runat="server" >
                            <asp:ListItem Text=" by City &nbsp;" Value="1"></asp:ListItem>
                            <asp:ListItem Text=" by Zip" Value="2" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="txtSearchZip" runat="server" onblur="populateZipDeps();" AutoPostBack="true" Width="92%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" TargetControlID="txtSearchZip" WatermarkCssClass="watermarked"
                                WatermarkText="                Enter search text here..." runat="server" ></cc1:TextBoxWatermarkExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSearchZip" InitialValue="" ErrorMessage="Please enter Zip..!">
                            &nbsp; <asp:Image ID="Image6" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter Zip ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                            <cc1:AutoCompleteExtender ServiceMethod="SearchZip"
                                MinimumPrefixLength="3"
                                CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" 
                                TargetControlID="txtSearchZip"
                                ID="AutoCompleteExtender1" runat="server" FirstRowSelected = "false">
                            </cc1:AutoCompleteExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="lblContentV2">
                            City
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="txtCity" Enabled="false" CssClass="watermarked" runat="server" Width="92%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="lblContentV2">
                            State
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="txtState" Enabled="false" CssClass="watermarked" runat="server" Width="92%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="lblContentV2">
                            Zip
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="txtZipResult" Enabled="false" CssClass="watermarked" runat="server" Width="92%"></asp:TextBox>
                        </td>
                    </tr>                    
            </table>
        </td>
        <td style="width:5%">
        </td>
        <td style="width:35%" valign="top">
            <table border="0" width="100%">
                <tr>
                    <td class="lblContentV2">
                        Work Phone
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtWorkPhone" runat="server" MaxLength="12" Width="80%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtWorkPhone" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td class="lblContentV2">
                        Cell Phone
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="12" Width="80%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtCellPhone" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td class="lblContentV2">
                        Primary Email Address
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtPrimaryEmail" runat="server" Width="80%"></asp:TextBox>                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPrimaryEmail" InitialValue="" ErrorMessage="Please enter email ..!">
                            &nbsp; <asp:Image ID="Image2" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter email ..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmail1" runat="server" Display="Dynamic"  ControlToValidate="txtPrimaryEmail" Text= "" ErrorMessage="" 
                            ValidationExpression ="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >
                            <asp:Image ID="Image3" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Invalid Email ..!" ImageUrl = "./images/err.jpg" />                           
                            </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="lblContentV2">
                        Alternate Email Address
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtAlternateEmail" runat="server" Width="80%"></asp:TextBox>                                                        
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"  ControlToValidate="txtAlternateEmail" Text= "" ErrorMessage="" 
                            ValidationExpression ="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >
                            <asp:Image ID="Image5" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Invalid Email ..!" ImageUrl = "./images/err.jpg" />                           
                            </asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </table>
    
    
    
    <table border = "0" width="100%">  
      <tr>
        <td colspan="2"><br /></td>
      </tr> 
      <tr>      
      <td align="right" style="width:92%">
          <asp:Button ID="btnSave" CssClass="btnstyle" runat="server" Text="Save" Width="100px" Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" TargetControlID="btnSave" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <asp:Button ID="btnProceed" CssClass="btnstyle" runat="server" CausesValidation="false" Text="PDF Report " Width="120px" Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender2" TargetControlID="btnProceed" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
            &nbsp;&nbsp;&nbsp;&nbsp;
          <asp:Button ID="btnSendMail" CssClass="btnstyle" runat="server" CausesValidation="false" Text="Send Mail" Width="120px" Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender3" TargetControlID="btnSendMail" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
          </td>
      <td style="width:8%"></td>
      </tr>
      <tr><td colspan="2">&nbsp;</td></tr>
      </table>  
      
 </td>
<td style="width:5%">
</td>
</tr>
</table>

</asp:Content>

