<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="SelectORG.aspx.vb" Inherits="WF.SelectORG" 
    title="Validate Organization" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>  


<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder"  runat="server">

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager> 

<%--<script type="text/javascript" id="validate" language="javascript">

    function validateTerms() {
        //alert("Hello World!");)
        //alert(document.getElementById("btnProceed"));
        //alert(document.getElementById('<%=chkAgreeRules.ClientID%>').checked);
        //alert(document.getElementById('<%=lblORGName.ClientID%>').innerHTML);


        if (document.getElementById('<%=chkAgreeRules.ClientID%>').checked && document.getElementById('<%=lblORGName.ClientID%>').innerHTML != "") {
            //alert(document.getElementById('<%=btnProceed.ClientID%>').disabled);

            document.getElementById('<%=btnProceed.ClientID%>').disabled = false;
         }
        else {
            //alert(document.getElementById('<%=btnProceed.ClientID%>').disabled);
            document.getElementById('<%=btnProceed.ClientID%>').disabled = true;
         }
        
    }

</script>--%>

<table width="100%">
<tr>
<td style="width:5%">
</td>
<td style="width:90%">

    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="100%">
     <tr>
        <td align="center">            
            <asp:Label ID="lblHeader" Font-Bold="true" Font-Size="18px" runat="server" Text="Validate Organization"></asp:Label>
        </td>
     </tr>
     <tr><td>&nbsp;</td></tr>
     <tr><td>&nbsp;</td></tr>
    </table>
    
    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="100%">
      <tr>
        <td>
        
        <table border="0" width="100%">   
        <%--<tr><td colspan="3"> &nbsp;</td> </tr>                    --%>
        <tr>          
          <td align="left" style="width:15%"></td>          
          <td style="width: 70%" align="center">
          <asp:Label runat="server" ID="lblBranchCode" class="lblContentV2" Text="Enter Organization Branch Code "></asp:Label>
          </td>
          <td style="width:15%"></td>
        </tr>
        <tr>          
          <td align="left" ></td>          
          <td  align="center">
          <asp:TextBox runat="server" ID="txtBranchCode" style="width:220px;"  MaxLength="10" ></asp:TextBox>
              <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBranchCode" FilterType="Numbers, UppercaseLetters, LowercaseLetters" >
              </cc1:FilteredTextBoxExtender>
              <cc1:TextBoxWatermarkExtender ID="WatermarkExtender1" TargetControlID="txtBranchCode" WatermarkCssClass="watermarked"
               WatermarkText="          Minimum of 6 characters" runat="server" >
              </cc1:TextBoxWatermarkExtender>
              <%--<cc1:BalloonPopupExtender ID="BalloonPopupExtender1" TargetControlID="txtBranchCode" BalloonStyle="Cloud"
                BalloonSize="Small" runat="server">
              </cc1:BalloonPopupExtender>--%>
          </td>
          <td></td>
        </tr>
        <tr>
            <td></td>
            <td align="center">
                <asp:RequiredFieldValidator ID="rfvtxtBranch" runat="server" ControlToValidate="txtBranchCode" ErrorMessage="Please enter branch code ..!"></asp:RequiredFieldValidator>                
                <asp:Label ID="lblValidationError" runat="server" Text="Sorry. We were unable to verify this Organization Code in our system. Please re-enter" ForeColor="Red" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>          
          <td align="left" ></td>          
          <td  align="center">
          <asp:Button ID="btnValidateORG" CssClass="btnstyle" runat="server" Text="Validate" Width="100px" Height="30px" />
            <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" TargetControlID="btnValidateORG" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
          </td>
          <td ></td>
        </tr>
        <tr><td colspan="3"> &nbsp;</td> </tr>
      </table>
        
            <table border="0" width="100%">                       
              <tr>
                <td align="left" valign="top" style="width:25%" class="lblContentV2">
                    Organization Name :
                </td>
                <td style="width:75%" valign="top">
                    <asp:Label ID="lblORGName" runat="server" text=""></asp:Label>                    
                </td>                
              </tr>
              <tr><td colspan="2">&nbsp;</td></tr>
              <tr>
                <td align="left" valign="top" style="width:25%" class="lblContentV2">
                    Organization Description :
                </td>
                <td style="width:75%" valign="top">
                    <asp:Label ID="lblORGDesc" runat="server" Text=""></asp:Label>
                </td>                
              </tr>
              <tr><td colspan="2">&nbsp;</td></tr>
              <tr>
                <td align="left" valign="top" style="width:25%" class="lblContentV2">
                    Organization Branch Name :
                </td>
                <td style="width:75%" valign="top">
                    <asp:Label ID="lblBranchName" runat="server" Text=""></asp:Label>
                </td>                
              </tr>
              <tr><td colspan="2">&nbsp;</td></tr>
              <tr>
                <td align="left" valign="top" style="width:25%" class="lblContentV2">
                    Organization Branch Address :
                </td>
                <td style="width:75%" valign="top">
                    <asp:Label ID="lblBranchAddr" runat="server" Text=""></asp:Label>
                </td>                
              </tr>
          </table>
        </td>
      </tr>
      <tr><td>&nbsp;</td></tr>
     <%-- <tr>
        <td>
            <div id="countrydivcontainer" style="border:1px solid gray; width:100%; margin-bottom: 1em; padding: 10px">    
                <table width="100%"><tr><td align="center">Terms & Conditions</td></tr></table>
                <p>1. By selecting Proceed button, you confirm that you belong to listed Organization & Branch. If not, please re-search for your correct branch details.</p>
                <p>2. A mail is sent to the respective authority to confirm veracity of the Complaint & only genuine candidates be considered for case processing.</p>
                <p>
                <asp:CheckBox ID="chkAgreeRules" Checked="false" runat="server" onchange="validateTerms();" Text=" &nbsp;&nbsp;I agree Terms & conditions." />
                </p>
                
            </div>
        </td>
      </tr>--%>
    </table>  
    
    
    
    <table border = "0" width="90%">      
      <tr>
      <td style="width:20%"></td>
      <td align="right" style="width:80%">
          <asp:Button ID="btnProceed" CssClass="btnstyle" runat="server" ToolTip="Select valid branch" Text="Continue" Width="120px" Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender2" TargetControlID="btnProceed" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
          </td>
      </tr>
      <tr><td colspan="2">&nbsp;</td></tr>
      </table>  
      
</td>
<td style="width:5%">
</td>
</tr>
</table>

</asp:Content>

