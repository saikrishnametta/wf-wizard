﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="IncidentDetails.aspx.vb" Inherits="WF.IncidentDetails" 
    title="Incident Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder"  runat="server">

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager> 

<script type="text/javascript" id="validate" language="javascript">
       
</script>

<table width="100%">
<tr>
<td style="width:5%">
</td>
<td style="width:90%">


    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="100%">
     <tr>
        <td align="center">            
            <asp:Label ID="lblHeader" Font-Bold="true" Font-Size="18px" runat="server" Text="Incident Details"></asp:Label>
        </td>
     </tr>
    </table>
    
    <table border="0" width="100%">
        <tr>
            <td colspan="3" class="lblContentV2">
                Please log individual incidents
            </td>
        </tr>            
        <tr>
            <td style="width:10%"></td>
            <td align="right" style="width:50%">                
                <asp:GridView ID="gvwIncidents" runat="server" BorderStyle="Solid" BorderWidth="1" Width="80%"                                 
                                  EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-VerticalAlign="Middle"                               
                                  CellPadding="4" EmptyDataText="No Incidents .." RowStyle-HorizontalAlign="Center"
                                  GridLines="Vertical" AutoGenerateColumns="False" DataKeyNames="IncidentID" 
                                  CssClass="gridview" AlternatingRowStyle-CssClass="even" 
                                  AllowSorting="False"  AllowPaging="True" PageSize="5">
                       <AlternatingRowStyle CssClass="even" />
                       <Columns>
                            <asp:BoundField  DataField="IncidentID" HeaderText="IncidentID" SortExpression="IncidentID" Visible="False"/> 
                            
                            <asp:TemplateField HeaderText="Serial No"  >
                                <ItemTemplate>    
                                   <%# CType(Container, GridViewRow).RowIndex + 1%>
                                </ItemTemplate>
                             </asp:TemplateField>                            
                             
                            <asp:TemplateField HeaderText="Date of Incident" SortExpression="DateOfIncident">                                
                                <ItemTemplate>
                                    <asp:Label ID="gvLblDateOfIncident" runat="server" Text='<%# Bind("DateOfIncident") %>' ></asp:Label>
                                </ItemTemplate>
                           </asp:TemplateField> 
                           <asp:TemplateField HeaderText="Incident Title" SortExpression="IncidentTitle">                                
                                <ItemTemplate>
                                    <asp:Label ID="gvLblIncidentTitle" runat="server" Text='<%# Bind("IncidentTitle") %>' ></asp:Label>
                                </ItemTemplate>
                           </asp:TemplateField>
		                   <asp:TemplateField >
                                    <ItemTemplate>
                                        <asp:LinkButton CommandName="Edit" CausesValidation="True" ValidationGroup="Test" ID="btnEdit" runat="server" Text="Edit"></asp:LinkButton>
                                    </ItemTemplate>                                    
                                    <ItemStyle HorizontalAlign="Left" />
                           </asp:TemplateField>                                    
                           <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete" 
                                    Runat="server"                                     
                                    OnClientClick="return confirm('Are you sure you want to delete this Incident?');" 
                                    CommandName="Delete" CausesValidation="False">Delete</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="Left" />                                
                           </asp:TemplateField>                                                  
                        </Columns>
                    </asp:GridView>
            </td>
            <td style="width:40%"></td>
        </tr>           
        <tr>
            <td style="width:10%"></td>
            <td align="right" style="width:50%">
                <asp:ImageButton ID="btnAddIncident" src="./images/AddIncident.png" Width="170px" Height="40px" runat="server" CausesValidation="false" /> 
            </td>
            <td style="width:40%"></td>
         </tr>
         <tr>
            <td colspan="3">&nbsp;</td>
         </tr>
    </table>
    
    
    <table border="0" style="background-color:#f1f1f1" width="100%">
    <tr>
        <td style="width:60%">
            <table border="0" width="100%">
                <tr>
                    <td colspan="3">
                        <asp:HiddenField ID="hdnIncidentID" runat="server" />
                    </td>
                 </tr>
                <tr>
                    <td colspan="3" class="lblContentV2">
                        Date of incident
                    </td>
                </tr>
                <tr>
                    <td style="width:35%" valign="middle">
                        <asp:TextBox ID="txtDtIncident" runat="server" style="width:100px;"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender1" 
                                TargetControlID="txtDtIncident" runat="server">
                            </cc1:CalendarExtender>                                                         
                            <asp:RequiredFieldValidator ID="rfvtxtDtIncident" runat="server" ControlToValidate="txtDtIncident" InitialValue="" ErrorMessage="Please enter incident date..!">
                            &nbsp; <asp:Image ID="ImgErrFname" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter incident date..!" ImageUrl = "./images/err.jpg" />
                            </asp:RequiredFieldValidator>
                    </td>
                    <td style="width:30%" valign="middle">                        
                    </td>
                    <td style="width:35%" valign="middle">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="lblContentV2">
                        Incident title
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                         <asp:TextBox ID="txtIncidentTitle" runat="server" style="width:100px;"></asp:TextBox> 
                    </td>
                </tr>
                 <tr>
                    <td colspan="3" class="lblContentV2">
                        Incident description (In 500 words or less…)
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="lblContent">
                        <asp:TextBox ID="txtIncidentDesr" Width="100%" runat="server" TextMode="MultiLine" Rows="5" ></asp:TextBox>
                           <cc1:RoundedCornersExtender ID="rndExtIncidentDesr" TargetControlID="txtIncidentDesr" Corners="All" Radius="9" runat="server">
                            </cc1:RoundedCornersExtender>
                    </td>
                </tr>                
            </table>
        </td>
        <td style="width:5%">
        </td>
        <td style="width:35%" valign="top">
            <table border="0" width="100%">
                <tr>
                        <td colspan="2" align="left" valign="top" class="lblContentV2">
                            <b><u>Attachments:</u></b>
                        </td>                        
                    </tr>  
                    <tr>
                        <td colspan="2" align="left" valign="top">
                            File 1 : <asp:FileUpload ID="fileUpload1" runat="server" />
                        </td>                        
                    </tr>           
                    <tr>
                        <td colspan="2" align="left" valign="top" >
                            File 2 : <asp:FileUpload ID="fileUpload2" runat="server" />
                        </td>                        
                    </tr>           
                    <tr>
                        <td colspan="2" align="left" valign="top" >
                            File 3 : <asp:FileUpload ID="fileUpload3" runat="server" />
                        </td>                        
                    </tr>  
                    <tr>
                        <td></td>
                        <td align="left" valign="top" class="lblContent">
                          <asp:Button ID="btnUpload" CssClass="btnstyle" runat="server" Text="Upload File" Width="140px" Height="25px" />
                          <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" TargetControlID="btnUpload" Corners="All" Radius="6" runat="server">
                              </cc1:RoundedCornersExtender>
                        </td>                        
                    </tr>
            </table>
        </td>
    </tr>
    </table>
    
    <table border = "0" width="100%">  
      <tr>
        <td colspan="2"><br /></td>
      </tr>   
      <tr>      
          <td align="right" style="width:92%">
              <asp:Button ID="btnSave" CssClass="btnstyle" runat="server" Text="Save" Width="100px" Height="25px" />
              <cc1:RoundedCornersExtender ID="RoundedCornersExtender2" TargetControlID="btnSave" Corners="All" Radius="6" runat="server">
                  </cc1:RoundedCornersExtender>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <asp:Button ID="btnProceed" CssClass="btnstyle" runat="server" CausesValidation="false" Text="Continue" OnClientClick="return confirm('Unsaved data will be discarded.  Are you sure you want to continue?');" Width="120px" Height="25px" />
              <cc1:RoundedCornersExtender ID="RoundedCornersExtender3" TargetControlID="btnProceed" Corners="All" Radius="6" runat="server">
                  </cc1:RoundedCornersExtender>
          </td>
        <td style="width:8%"></td>
      </tr>
      <tr><td colspan="2">&nbsp;</td></tr>
    </table>  
    
</td>
<td style="width:5%">
</td>
</tr>
</table>      
      
</asp:Content>