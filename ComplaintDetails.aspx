﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="ComplaintDetails.aspx.vb" Inherits="WF.ComplaintDetails" 
    title="Complaint Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder"  runat="server">

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager> 

<script type="text/javascript" id="validate" language="javascript">

    function enableAtornyDep() {

        //alert(document.getElementById('<%=ddlDoUHaveAttorny.ClientID%>').value);

        if (document.getElementById('<%=ddlDoUHaveAttorny.ClientID%>').value == "38") {
            document.getElementById('<%=ddlContactedAttorny.ClientID%>').disabled = false;            
        }
        else {
            document.getElementById('<%=ddlContactedAttorny.ClientID%>').disabled = true;
            document.getElementById('<%=ddlContactedAttorny.ClientID%>').options[0].selected = true;
        }
    }


    function enableStillEmployedDep() {

        //alert(document.getElementById('<%=ddlStillEmployed.ClientID%>').value);

        if (document.getElementById('<%=ddlStillEmployed.ClientID%>').value == "45") {
        
            document.getElementById('<%=txtDtEmplmtEnd.ClientID%>').disabled = false;
            document.getElementById('<%=ddlHowEmplEnd.ClientID%>').disabled = false;
        }
        else {

            document.getElementById('<%=txtDtEmplmtEnd.ClientID%>').disabled = true;
            document.getElementById('<%=txtDtEmplmtEnd.ClientID%>').value = ""
            
            document.getElementById('<%=ddlHowEmplEnd.ClientID%>').disabled = true;
            document.getElementById('<%=ddlHowEmplEnd.ClientID%>').options[0].selected = true;
        }
    }


    function enableFiledComplaintDep() {

        //alert(document.getElementById('<%=ddlFiledComplaint.ClientID%>').value);

        if (document.getElementById('<%=ddlFiledComplaint.ClientID%>').value == "42") {

            document.getElementById('<%=txtComplaintAgency.ClientID%>').disabled = false;
            document.getElementById('<%=txtDtComplaintFiled.ClientID%>').disabled = false;
        }
        else {

            document.getElementById('<%=txtComplaintAgency.ClientID%>').disabled = true;
            document.getElementById('<%=txtComplaintAgency.ClientID%>').value = ""

            document.getElementById('<%=txtDtComplaintFiled.ClientID%>').disabled = true;
            document.getElementById('<%=txtDtComplaintFiled.ClientID%>').value = ""
        }
    }
    
</script>

<table width="100%">
<tr>
<td style="width:5%">
</td>
<td style="width:90%">

    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="100%">
     <tr>
        <td align="center">            
            <asp:Label ID="Label1" Font-Bold="true" Font-Size="18px" runat="server" Text="Complaint Details"></asp:Label>
        </td>
     </tr>
    </table>    
    
    
    <table border="0" width="100%">
    <tr>
        <td style="width:60%">
            <table border="0" width="100%">
                <tr>
                    <td class="lblContent">
                        Your job title
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtJobTitle" runat="server" Width="92%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblContent">
                        Please give approximate number of all employees (full time & part) at all employer locations
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlNoOfEmps" runat="server" Width="92%">
                            <asp:ListItem Text="&nbsp;&nbsp;Select ..." Value="-1"></asp:ListItem>
                            <asp:ListItem Text="4-14" Value="48"></asp:ListItem>
                            <asp:ListItem Text="15-19" Value="49"></asp:ListItem>
                            <asp:ListItem Text="20-100" Value="50"></asp:ListItem>
                            <asp:ListItem Text="101-200" Value="51"></asp:ListItem>
                            <asp:ListItem Text="201-500" Value="52"></asp:ListItem>
                            <asp:ListItem Text="500+" Value="53"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="lblContent">
                        Do you have an attorney ?
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlDoUHaveAttorny" onchange="enableAtornyDep();" Width="92%" AutoPostBack="true" runat="server">
                            <asp:ListItem Text="&nbsp;&nbsp;Select ..." Value="-1"></asp:ListItem>
                            <asp:ListItem Text="Yes" Value="38"></asp:ListItem>
                            <asp:ListItem Text="No" Value="39"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width:5%">
        </td>
        <td style="width:35%" valign="top">
        </td>
     </tr>
     </table>
     
   <table runat="server" id="tblAtrnyContact" visible="false" width="100%">
   <tr><td>&nbsp;</td></tr>
   <tr>
       <td>
            <table border="0" style="background-color:#f1f1f1"  width="100%">    
            <tr>
                <td style="width:60%">
                    <table border="0" width="100%">
                        <tr><td colspan="3" align="left" class="lblContactHeader">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Attorney Details</td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr>
                            <td colspan="3">
                                <asp:HiddenField ID="hdnContactID" runat="server" />
                            </td>
                         </tr>
                        <tr>
                            <td colspan="3" class="lblContentV2">
                                Name
                            </td>
                        </tr>
                        <tr>
                            <td style="width:35%" valign="middle">
                                <asp:TextBox ID="txtFName" MaxLength="100" runat="server" Width="78%" ></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="txtFName" WatermarkCssClass="watermarked"
                                        WatermarkText=" First" runat="server" ></cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFName" FilterType="UppercaseLetters, LowercaseLetters" >
                                    </cc1:FilteredTextBoxExtender>                                                   
                                    <asp:RequiredFieldValidator ID="rfvtxtFName" runat="server" ControlToValidate="txtFName" InitialValue="" ErrorMessage="Please enter first name ..!">
                                    &nbsp; <asp:Image ID="ImgErrFname" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter first name ..!" ImageUrl = "./images/err.jpg" />
                                    </asp:RequiredFieldValidator>
                            </td>
                            <td style="width:30%" valign="middle">
                                <asp:TextBox ID="txtMName" runat="server" MaxLength="100" ></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtMName" FilterType="UppercaseLetters, LowercaseLetters" >
                                    </cc1:FilteredTextBoxExtender>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" TargetControlID="txtMName" WatermarkCssClass="watermarked"
                                        WatermarkText=" Middle" runat="server" ></cc1:TextBoxWatermarkExtender>
                            </td>
                            <td style="width:35%" valign="middle">
                                <asp:TextBox ID="txtLName" runat="server" MaxLength="100" Width="78%"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" TargetControlID="txtLName" WatermarkCssClass="watermarked"
                                        WatermarkText=" Last" runat="server" ></cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtLName" FilterType="UppercaseLetters, LowercaseLetters" >
                                    </cc1:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="rfvtxtLName" runat="server" ControlToValidate="txtLName" InitialValue="" ErrorMessage="Please enter last name ..!">
                                    &nbsp; <asp:Image ID="ImgErrLName" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter last name ..!" ImageUrl = "./images/err.jpg" />
                                    </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                         <tr>
                            <td colspan="3" class="lblContentV2">
                                Address
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:TextBox ID="txtAddress1" runat="server" Width="92%" ></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtAddress1" WatermarkCssClass="watermarked"
                                        WatermarkText="                Line1" runat="server" ></cc1:TextBoxWatermarkExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAddress1" InitialValue="" ErrorMessage="Please fill address..!">
                                    &nbsp; <asp:Image ID="Image1" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please fill address ..!" ImageUrl = "./images/err.jpg" />
                                    </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:TextBox ID="txtAddress2" runat="server" Width="92%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" TargetControlID="txtAddress2" WatermarkCssClass="watermarked"
                                        WatermarkText="                Line2" runat="server" ></cc1:TextBoxWatermarkExtender>
                            </td>                    
                        </tr>
                        <tr>
                            <td colspan="3" class="lblContentV2">
                                Search for City
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="lblContentV2">
                              <asp:RadioButtonList ID="rdgZip" repeatdirection="Horizontal" runat="server" >
                                    <asp:ListItem Text=" by City &nbsp;" Value="1"></asp:ListItem>
                                    <asp:ListItem Text=" by Zip" Value="2" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox ID="txtSearchZip" runat="server" AutoPostBack="true" Width="92%"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" TargetControlID="txtSearchZip" WatermarkCssClass="watermarked"
                                        WatermarkText="                Enter search text here..." runat="server" ></cc1:TextBoxWatermarkExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSearchZip" InitialValue="" ErrorMessage="Please enter Zip..!">
                                    &nbsp; <asp:Image ID="Image6" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter Zip ..!" ImageUrl = "./images/err.jpg" />
                                    </asp:RequiredFieldValidator>
                                    <cc1:AutoCompleteExtender ServiceMethod="SearchZip"
                                        MinimumPrefixLength="3"
                                        CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" 
                                        TargetControlID="txtSearchZip"
                                        ID="AutoCompleteExtender1" runat="server" FirstRowSelected = "false">
                                    </cc1:AutoCompleteExtender>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="lblContentV2">
                                    City
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox ID="txtCity" Enabled="false" CssClass="watermarked" runat="server" Width="92%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="lblContentV2">
                                    State
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox ID="txtState" Enabled="false" CssClass="watermarked" runat="server" Width="92%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="lblContentV2">
                                    Zip
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox ID="txtZipResult" Enabled="false" CssClass="watermarked" runat="server" Width="92%"></asp:TextBox>
                                </td>
                            </tr>         
                    </table>
                </td>
                <td style="width:5%">
                </td>
                <td style="width:35%" valign="top">
                    <table border="0" width="100%">
                         <tr>
                            <td class="lblContentV2">
                                Home Phone
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtHomePhone" runat="server" MaxLength="12" Width="80%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtHomePhone" FilterType="Numbers" >
                                    </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblContentV2">
                                Work Phone
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtWorkPhone" runat="server" MaxLength="12" Width="80%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtWorkPhone" FilterType="Numbers" >
                                    </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblContentV2">
                                Cell Phone
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="12" Width="80%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtCellPhone" FilterType="Numbers" >
                                    </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblContentV2">
                                Primary Email Address
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPrimaryEmail" runat="server" Width="80%"></asp:TextBox>                            
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPrimaryEmail" InitialValue="" ErrorMessage="Please enter email ..!">
                                    &nbsp; <asp:Image ID="Image2" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Please enter email ..!" ImageUrl = "./images/err.jpg" />
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail1" runat="server" Display="Dynamic"  ControlToValidate="txtPrimaryEmail" Text= "" ErrorMessage="" 
                                    ValidationExpression ="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >
                                    <asp:Image ID="Image3" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Invalid Email ..!" ImageUrl = "./images/err.jpg" />                           
                                    </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblContentV2">
                                Alternate Email Address
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtAlternateEmail" runat="server" Width="80%"></asp:TextBox>                                                        
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"  ControlToValidate="txtAlternateEmail" Text= "" ErrorMessage="" 
                                    ValidationExpression ="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >
                                    <asp:Image ID="Image5" style="vertical-align:bottom" runat="server" CssClass="errImgstyle" ToolTip="Invalid Email ..!" ImageUrl = "./images/err.jpg" />                           
                                    </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
       
       </td>
   </tr>
   <tr><td>&nbsp;</td></tr>
   </table>
    
    
    <table border="0" width="100%">
    <tr>
        <td style="width:50%" class="lblContent">
            Have you contacted your attorney regarding this matter?
        </td>
        <td style="width:5%">
        </td>
        <td style="width:55%" valign="top">
             <asp:DropDownList ID="ddlContactedAttorny" Enabled="false" Width="40%" runat="server">
                <asp:ListItem Text="&nbsp;&nbsp;Select ..." Value="-1"></asp:ListItem>
                <asp:ListItem Text="Yes" Value="40"></asp:ListItem>
                <asp:ListItem Text="No" Value="41"></asp:ListItem>
            </asp:DropDownList>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            How are you associated with the Organization            
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:DropDownList ID="ddlORGAssociation" runat="server" Width="40%">
                <asp:ListItem Text="&nbsp;&nbsp;Select ..." Value="-1"></asp:ListItem>
                <asp:ListItem Text="Customer" Value="35"></asp:ListItem>
                <asp:ListItem Text="Employee" Value="36"></asp:ListItem>
                <asp:ListItem Text="Former Employee" Value="37"></asp:ListItem>
            </asp:DropDownList>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            What is your hire date or application date
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:TextBox ID="txtHireDate" runat="server" Width="40%"></asp:TextBox>
            <cc1:CalendarExtender ID="CalExtHireDate" TargetControlID="txtHireDate" runat="server">
            </cc1:CalendarExtender>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            Are you still employed by the organization
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:DropDownList ID="ddlStillEmployed" onchange="enableStillEmployedDep();" runat="server" Width="40%">
                <asp:ListItem Text="&nbsp;&nbsp;Select ..." Value="-1"></asp:ListItem>
                <asp:ListItem Text="Yes" Value="44"></asp:ListItem>
                <asp:ListItem Text="No" Value="45"></asp:ListItem>
            </asp:DropDownList>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            If No, when did your employment end ?
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:TextBox ID="txtDtEmplmtEnd" runat="server" Enabled="false" Width="40%"></asp:TextBox>
            <cc1:CalendarExtender ID="CalExtDtEmplmtEnd" TargetControlID="txtDtEmplmtEnd" runat="server">
            </cc1:CalendarExtender>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            If No, how did your employment end ?
        </td>
        <td >
        </td>
        <td valign="top">
             <asp:DropDownList ID="ddlHowEmplEnd" Enabled="false" runat="server" Width="40%">
                <asp:ListItem Text="&nbsp;&nbsp;Select ..." Value="-1"></asp:ListItem>
                <asp:ListItem Text="Terminated" Value="46"></asp:ListItem>
                <asp:ListItem Text="Quit" Value="47"></asp:ListItem>
            </asp:DropDownList>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            Have you filed this complaint with any federal, state or local anti-discriminatory agency ?
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:DropDownList ID="ddlFiledComplaint" onchange="enableFiledComplaintDep();" runat="server" Width="40%">
                <asp:ListItem Text="&nbsp;&nbsp;Select ..." Value="-1"></asp:ListItem>
                <asp:ListItem Text="Yes" Value="42"></asp:ListItem>
                <asp:ListItem Text="No" Value="43"></asp:ListItem>
            </asp:DropDownList>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            If Yes, what agency ?
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:TextBox ID="txtComplaintAgency" runat="server" Enabled="false" Width="40%"></asp:TextBox>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            If Yes, when was the complaint filed ?
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:TextBox ID="txtDtComplaintFiled" runat="server" Enabled="false" Width="40%"></asp:TextBox>
            <cc1:CalendarExtender ID="CalExtDtComplaintFiled" TargetControlID="txtDtComplaintFiled" runat="server">
            </cc1:CalendarExtender>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            Please provide a title for this complaint that describes the nature of complaint in less than 25 words
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:TextBox ID="txtComplaintTitle" runat="server" Rows="3" Width="40%" TextMode="MultiLine"></asp:TextBox>
        </td>
     </tr>
     <tr>
        <td class="lblContent">
            Please provide details of the  complaint in less than 500 characters.
        </td>
        <td >
        </td>
        <td valign="top">
            <asp:TextBox ID="txtComplaintDesr" runat="server" Rows="5" Width="40%" TextMode="MultiLine"></asp:TextBox>
        </td>
     </tr>
     </table>
    
    
    <table border = "0" width="100%">  
      <%--<tr>
        <td colspan="2"><br /></td>
      </tr> --%>     
      <tr>      
      <td align="right" style="width:92%">
          <asp:Button ID="btnProceed" CssClass="btnstyle" runat="server" Text="Save & Continue" Width="140px" Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender2" TargetControlID="btnProceed" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
          </td>
      <td style="width:8%"></td>
      </tr>
      <tr><td colspan="2">&nbsp;</td></tr>
      </table>  
      
</td>
<td style="width:5%">
</td>
</tr>
</table>

</asp:Content>
