﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Friend-Relative.aspx.vb" Inherits="WF.Friend_Relative" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Friend-Relative</title>
    <link href="fontStyles.css" type="text/css" rel="stylesheet" />
</head>
<body class="body">
    <form id="form1" runat="server">
     
     <table border = "0" cellpadding="20" width="90%">
        <tr>
            <td align="center">
            
                    <table border="2" cellpadding="4" cellspacing="0"  align="left" width="90%"> 
                   <tr>
                     <td>
                         <table class="PageMainTable" border="0" >
                            <tr>
                                <td class="PageHeader" align="center" valign="middle" rowspan="2" >
                                         <asp:Label ID="Label2" runat="server" ForeColor="white" Font-Bold="true"
                                               Text="WELCOME NETWORK INCIDENT REPORT WIZARD">
                                    </asp:Label>
                                </td>
                            </tr>
                     
                            <tr>
                                <td valign="middle">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td valign="middle" align="center">
                                    <asp:Label ID="Label10" runat="server" ForeColor="white" Font-Bold="false"
                                               Text="Friend-Relative Contact">
                                    </asp:Label>
                                </td>
                            </tr>
                         </table>
                     </td>
                   </tr>
                   <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="accordionHeader" align="left" width="25%" >
                                    FRIEND RELATIVE INFORMATION
                                </td>
                                <td width="50%">
                                
                                </td>
                                <td class="accordionHeader" align="center" width="75%">
                                    Type : Relative Or Friend
                                </td>
                            </tr>
                        </table>
                    </td>
                   </tr>
                   
               <tr>
               
               <td class="body">
              
              <table>
                <tr>
                    <td>&nbsp;</td>
                </tr>
              </table>
              
              <table border= "0" width="100%">
                <tr>
                    <td>
                        <table border="0" width = "100%">
                            <tr>
                                <td align="left" width="7%">
                                    Firstname:
                                </td>
                                <td width="18%" align="left">
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" width="8%">
                                    Middlename:
                                </td>
                                <td width="17%">
                                    <asp:TextBox ID="txtMName" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    LastName:
                                </td>
                                <td width="17%">
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                </td>
                                <td width="25%" align="left">
                                    Prefix
                                    <asp:DropDownList ID="ddlPrefix" runat="server">
                                        <asp:ListItem Text="Mr."></asp:ListItem>
                                        <asp:ListItem Text="Ms."></asp:ListItem>
                                        <asp:ListItem Text="Dr."></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td width = "7%" align="left">
                                    Address:
                                </td>
                                <td width = "17%" align="left">
                                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                                <td width = "9%" align="left">
                                    City:
                                </td>
                                <td width = "17%" align="left">
                                    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                </td>
                                <td width = "8%" align="left">
                                    State:
                                </td>
                                <td width = "17%">
                                    <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
                                </td>                                                   
                                <td width="25%" align="left">
                                    ZIP <asp:TextBox ID="txtZip" Width="20%" runat="server"></asp:TextBox> &nbsp;
                                    Country: <asp:TextBox ID="txtCountry" Width="35%" runat="server"></asp:TextBox>                            
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td width="7%" align="left">
                                    Email:
                                </td>
                                <td width="18%" align="left">
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    HomePhone:
                                </td>
                                <td width="17%" align="left">
                                    <asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    CellPhone:
                                </td>
                                <td width="17%" align="left">
                                    <asp:TextBox ID="txtCellPhone" runat="server"></asp:TextBox>
                                </td>
                                <td width="25%" align="left">
                                    Workphone: <asp:TextBox ID="txtWorkPhone" runat="server"></asp:TextBox>
                                </td>                               
                            </tr>
                        </table>
                    </td>                    
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td WIDTH="50%" class="accordionHeader" align = "center">GRID OF FRIEDS WITH ADD & DELETE BUTTONS COMES HERE</td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                                                
                <tr>
                   <td>
                    <table>
                        <tr>
                            <td width = "30%">Complaint ID Number:  xx-xxxx-xx
                            </td>
                            <td width = "30%">Barcode Number: 
                            </td>
                            <td width = "40%" align="middle"><asp:Label ID="lblDate" Text="Creation date:" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                   </td>
                </tr>
                
                
              </table>
              
              <table border = "0" width="95%">
              <tr><td align="right" class="style2">
                  <asp:Button ID="btnProceed" runat="server" Text="Proceed" Width="81px" />
                  </td></tr></table>
        </td>
        </tr>
        
        </table>
            </td>
        </tr>
     </table>
     
     
    </form>
</body>
</html>
