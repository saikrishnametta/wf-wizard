﻿<%@ Page Title="TimeOut" Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="TimeOut.aspx.vb" Inherits="WF.TimeOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder"  runat="server">



<script type="text/javascript" id="validate" language="javascript">

      

</script>

<table  width="100%">
<tr>
<td style="width:5%">
</td>
<td style="width:90%">

<br />
<br />
<br />
   <p class="normal"><b>Your session has timed out and you have been successfully 
  logged out of the system.</b></p>
  <br />
  <p class="normal">In an effort to retain the confidentiality of your account information, 
  you have been automatically logged off. This occurs when there is no browser 
  activity for an extended period of time. Any screens that where not submitted 
  using the &quot;Finish&quot; button were not processed. You will be redirected 
  to home page shortly.</p>
<p class="normal">Please click <a href=LogIn.aspx target=_parent>here</a> to return 
  to the log in page.</p>
      
 </td>
<td style="width:5%">
</td>
</tr>
</table>

</asp:Content>

