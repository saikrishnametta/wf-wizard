﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ComplainantInfo.aspx.vb" Inherits="WF.ComplainantInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Complainant Information</title>
    <link href="fontStyles.css" type="text/css" rel="stylesheet" />
</head>
<body class="body">
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <table border = "0" cellpadding="20" width="90%">
        <tr>
            <td align="center">
            
                    <table border="2" cellpadding="4" cellspacing="0"  align="left" width="90%"> 
                   <tr>
                     <td>
                         <table class="PageMainTable" border="0" >
                            <tr>
                                <td class="PageHeader" align="center" valign="middle" rowspan="2" >
                                         <asp:Label ID="Label2" runat="server" ForeColor="white" Font-Bold="true"
                                               Text="WELCOME NETWORK INCIDENT REPORT WIZARD">
                                    </asp:Label>
                                </td>
                            </tr>
                     
                            <tr>
                                <td valign="middle">&nbsp;</td>
                            </tr>
                            
                            <tr>
                                <td valign="middle" align="center">
                                    <asp:Label ID="Label10" runat="server" ForeColor="white" Font-Bold="false"
                                               Text="Complainant Information">
                                    </asp:Label>
                                </td>
                            </tr>
                         </table>
                     </td>
                   </tr>
                   <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="accordionHeader" width="20%" >
                                    PERSONAL INFORMATION
                                </td>
                                <td width="50%">
                                
                                </td>
                                <td class="accordionHeader" align="center" width="30%">
                                    Type : Customer or Employee Complaint
                                </td>
                            </tr>
                        </table>
                    </td>
                   </tr>
                   
               <tr>
               
               <td class="body">
              
              <table>
                <tr>
                    <td>&nbsp;</td>
                </tr>
              </table>
              
              <table border= "0" width="100%">
                <tr>
                    <td>
                        <table border="0" width = "100%">
                            <tr>
                                <td align="left" width="7%">
                                    Firstname:
                                </td>
                                <td width="18%" align="left">
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" width="8%">
                                    Middlename:
                                </td>
                                <td width="17%">
                                    <asp:TextBox ID="txtMName" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    LastName:
                                </td>
                                <td width="17%">
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                </td>
                                <td width="25%" align="left">
                                    Prefix
                                    <asp:DropDownList ID="ddlPrefix" runat="server">
                                        <asp:ListItem Text="Mr." Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Ms." Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Dr." Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td width = "7%" align="left">
                                    Address:
                                </td>
                                <td width = "17%" align="left">
                                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                                <td width = "9%" align="left">
                                    City:
                                </td>
                                <td width = "17%" align="left">
                                    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                </td>
                                <td width = "8%" align="left">
                                    State:
                                </td>
                                <td width = "17%">
                                    <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
                                </td>                                                   
                                <td width="25%" align="left">
                                    ZIP <asp:TextBox ID="txtZip" Width="20%" runat="server"></asp:TextBox> &nbsp;
                                    Country: <asp:TextBox ID="txtCountry" Width="35%" MaxLength="2" runat="server"></asp:TextBox>                            
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td width="7%" align="left">
                                    Email:
                                </td>
                                <td width="18%" align="left">
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    HomePhone:
                                </td>
                                <td width="17%" align="left">
                                    <asp:TextBox ID="txtHomePhone" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    CellPhone:
                                </td>
                                <td width="17%" align="left">
                                    <asp:TextBox ID="txtCellPhone" runat="server"></asp:TextBox>
                                </td>
                                <td width="25%" align="left">
                                    Workphone: <asp:TextBox ID="txtWorkPhone" runat="server"></asp:TextBox>
                                </td>                               
                            </tr>
                        </table>
                    </td>                    
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <table width="100%" border="0">
                            
                            <tr>
                                <td width="38%" align="left">
                                    Have you contacted an Attorney concerning this matter?
                                </td>
                                <td width="62%" align="left" >
                                    <asp:CheckBox ID="chkContactedAtrn" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="38%" align="left">
                                    Do you have an Attorney?
                                </td>
                                <td width="62%" align="left" >
                                    <asp:CheckBox ID="chkHaveAtrn" runat="server" />
                                </td>
                            </tr>
                            <%--<tr>
                                <td colspan="2">
                                &nbsp;
                                </td>
                            </tr>--%>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="accordionHeader" width="20%" align="left">
                                    ATTORNEY INFORMATION
                                </td>
                                <td width="20%"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <table border="0" width = "100%">
                            <tr>
                                <td align="left" width="7%">
                                    Firstname:
                                </td>
                                <td width="18%" align="left">
                                    <asp:TextBox ID="txtAtrnFName" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" width="8%">
                                    Middlename:
                                </td>
                                <td width="17%">
                                    <asp:TextBox ID="txtAtrnMName" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    LastName:
                                </td>
                                <td width="17%">
                                    <asp:TextBox ID="txtAtrnLName" runat="server"></asp:TextBox>
                                </td>
                                <td width="25%" align="left">
                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td width = "7%" align="left">
                                    Address:
                                </td>
                                <td width = "17%" align="left">
                                    <asp:TextBox ID="txtAtrnAddress" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </td>
                                <td width = "9%" align="left">
                                    City:
                                </td>
                                <td width = "17%" align="left">
                                    <asp:TextBox ID="txtAtrnCity" runat="server"></asp:TextBox>
                                </td>
                                <td width = "8%" align="left">
                                    State:
                                </td>
                                <td width = "17%">
                                    <asp:TextBox ID="txtAtrnState" runat="server"></asp:TextBox>
                                </td>                                                   
                                <td width="25%" align="left">
                                    ZIP <asp:TextBox ID="txtAtrnZip" Width="20%" runat="server"></asp:TextBox> &nbsp;
                                    Country: <asp:TextBox ID="txtAtrnCountry" Width="35%" MaxLength="2" runat="server"></asp:TextBox>                            
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td width="7%" align="left">
                                    Email:
                                </td>
                                <td width="18%" align="left">
                                    <asp:TextBox ID="txtAtrnEmail" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    Workphone:
                                </td>
                                <td width="17%" align="left">
                                    <asp:TextBox ID="txtAtrnWorkPhone" runat="server"></asp:TextBox>
                                </td>
                                <td width="8%" align="left">
                                    CellPhone:
                                </td>
                                <td width="17%" align="left">
                                    <asp:TextBox ID="txtAtrnCellPhone" runat="server"></asp:TextBox>
                                </td>
                                <td width="25%" align="left">
                                    
                                </td>                               
                            </tr>
                        </table>
                    </td>                    
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                                
                <tr>
                   <td>
                    <table>
                        <tr>
                            <td width = "30%">Complaint ID Number:  xx-xxxx-xx
                            </td>
                            <td width = "30%">Barcode Number: 
                            </td>
                            <td width = "40%" align="middle"><asp:Label ID="lblDate" Text="Creation date:" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                   </td>
                </tr>
                
                
              </table>
              
              <table border = "0" width="95%">
              <tr><td align="right" class="style2">
                  <asp:Button ID="btnProceed" runat="server" Text="Proceed" Width="81px" />
                  </td></tr></table>
        </td>
        </tr>
        
        </table>
            </td>
        </tr>
     </table>
     
     
    </form>
</body>

</html>
