﻿Imports WF.WatchForce.Data
Imports System.Data.SqlClient

Partial Public Class HarassmentDetails
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtHarasee, dtItems As DataTable
    Dim DBConn As DBSql
    Dim ComplaintID, ContactID As Int64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")

            If Not Page.IsPostBack Then
                LoadValues()
                ViewState("Mode") = "Add"
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub


    Private Sub LoadValues()
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter

        Try

            dtHarasee = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spGetHarasserContacts"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ComplaintID"
            param.DbType = DbType.Int64
            param.Value = Session("ComplaintID")

            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtHarasee)
            If Not dtHarasee Is Nothing AndAlso dtHarasee.Rows.Count > 0 Then
                gvwHarassContacts.DataSource = dtHarasee.DefaultView
                gvwHarassContacts.DataBind()

                dtHarasee.PrimaryKey = New DataColumn() {dtHarasee.Columns("ContactID")}

                ViewState("dtHarasee") = dtHarasee
            Else
                gvwHarassContacts.EmptyDataText = "There is no setup available"
            End If

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Try
            Response.Redirect("IncidentDetails.aspx", False)
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub


    Private Sub populateZip()
        Dim strSql As String
        Dim objCmd As SqlClient.SqlCommand
        Try


            strSql = "SELECT " & _
                     "ISNULL(CONVERT(VARCHAR,ZIP),'') + '  ' + " & _
                     "ISNULL(CONVERT(VARCHAR,Primary_City),'') + '  ' + " & _
                     "ISNULL(CONVERT(VARCHAR,State),'') ZipFull " & _
                                "FROM DBO.ZIPCODE " & _
                     "WHERE ZIP LIKE @ZIP " & _
                     "ORDER BY ZIP"

            objCmd = New SqlClient.SqlCommand()
            objCmd.Connection = New SqlClient.SqlConnection(DBConn.DBConnection)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = strSql
            objCmd.Parameters.AddWithValue("@ZIP", "890" + "%")

            Dim objAdapter As New SqlDataAdapter()
            dtItems = New DataTable()

            objAdapter.SelectCommand = objCmd
            'objAdapter.SelectCommand.Connection = mobjConnection
            objAdapter.SelectCommand.CommandTimeout = 0
            objAdapter.Fill(dtItems)
            objAdapter.Dispose()
            objAdapter = Nothing

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub


    'Protected Sub zipSelected(ByVal sender As Object, ByVal e As EventArgs) Handles txtZip.TextChanged
    '    Dim strSql As String
    '    Dim objCmd As SqlClient.SqlCommand
    '    Dim strZip As String
    '    Try
    '        If (txtZip.Text.Trim() <> String.Empty) Then

    '            strZip = txtZip.Text.Trim().Substring(0, txtZip.Text.Trim().IndexOf(" "))

    '            strSql = "SELECT ISNULL(CONVERT(VARCHAR,ZIP),'') ZIP, ISNULL(CONVERT(VARCHAR,Primary_City),'') Primary_City, ISNULL(CONVERT(VARCHAR,State),'') State FROM DBO.ZIPCODE WHERE ZIP = " & strZip

    '            objCmd = New SqlClient.SqlCommand()
    '            objCmd.Connection = New SqlClient.SqlConnection(DBConn.DBConnection)
    '            objCmd.CommandType = CommandType.Text
    '            objCmd.CommandText = strSql

    '            Dim objAdapter As New SqlDataAdapter()
    '            dtItems = New DataTable()

    '            objAdapter.SelectCommand = objCmd
    '            objAdapter.SelectCommand.CommandTimeout = 0
    '            objAdapter.Fill(dtItems)
    '            objAdapter.Dispose()
    '            objAdapter = Nothing

    '            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
    '                lblCity.Text = dtItems.Rows(0)("Primary_City")
    '                lblState.Text = dtItems.Rows(0)("State")
    '                lblZip.Text = dtItems.Rows(0)("ZIP")
    '            End If
    '        Else
    '            lblCity.Text = "-"
    '            lblState.Text = "-"
    '            lblZip.Text = "-"
    '        End If

    '    Catch ex As Exception
    '        Console.Write(ex.Message)
    '        'ObjError.LogError(ex)
    '    End Try
    'End Sub

    <System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()> Public Shared Function SearchZip(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim strSql As String
        Dim objCmd As SqlClient.SqlCommand
        Dim dtItems As DataTable
        Try


            strSql = "SELECT " & _
                     "ISNULL(CONVERT(VARCHAR,ZIP),'') + ' / ' + " & _
                     "ISNULL(CONVERT(VARCHAR,Primary_City),'') + ' / ' + " & _
                     "ISNULL(CONVERT(VARCHAR,State),'') ZipFull " & _
                                "FROM DBO.ZIPCODE " & _
                     "WHERE ZIP LIKE @ZIP " & _
                     "ORDER BY ZIP"

            objCmd = New SqlClient.SqlCommand()
            objCmd.Connection = New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("WFConnString").ConnectionString)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = strSql
            objCmd.Parameters.AddWithValue("@ZIP", prefixText + "%")

            Dim objAdapter As New SqlDataAdapter()
            dtItems = New DataTable()

            objAdapter.SelectCommand = objCmd
            'objAdapter.SelectCommand.Connection = mobjConnection
            objAdapter.SelectCommand.CommandTimeout = 0
            objAdapter.Fill(dtItems)
            objAdapter.Dispose()
            objAdapter = Nothing

            Dim Zips As List(Of String) = New List(Of String)
            For Each dtrow In dtItems.Rows
                Zips.Add(dtrow("ZipFull").ToString)
            Next

            Return Zips

        Catch ex As Exception
            Console.Write(ex.Message)
            'ObjError.LogError(ex)
        End Try

    End Function


    Private Sub gvwHarassContacts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwHarassContacts.RowCommand

    End Sub

    Private Sub gvwHarassContacts_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvwHarassContacts.RowDeleting
        Dim ContactId As Int64
        Try
            'ContactId = gvwHarassContacts.Rows(gvwHarassContacts.EditIndex).FindControl("ContactID").
            If gvwHarassContacts.DataKeys.Count > 0 Then
                ContactId = gvwHarassContacts.DataKeys(e.RowIndex)(0).ToString()
                DeleteContact(ContactId)
            End If

        Catch ex As Exception

        End Try
    End Sub


    Private Sub gvwHarassContacts_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvwHarassContacts.RowEditing
        Dim ContactId As Int64
        Try
            'ContactId = gvwHarassContacts.Rows(gvwHarassContacts.EditIndex).FindControl("ContactID").
            If gvwHarassContacts.DataKeys.Count > 0 Then
                ContactId = gvwHarassContacts.DataKeys(e.NewEditIndex)(0).ToString()
                pickContact(ContactId)
                ViewState("Mode") = "Update"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub pickContact(ByVal ContactID As Int64)
        Dim drHarasee As DataRow
        Try
            dtHarasee = ViewState("dtHarasee")
            If Not dtHarasee Is Nothing AndAlso dtHarasee.Rows.Count > 0 Then
                drHarasee = dtHarasee.Rows.Find(New Object() {ContactID})
                If Not drHarasee Is Nothing Then
                    hdnContactID.Value = drHarasee("ContactID")
                    txtFName.Text = drHarasee("FirstName")
                    txtMName.Text = drHarasee("MiddleName")
                    txtLName.Text = drHarasee("LastName")
                    txtJobTitle.Text = drHarasee("JobTitle")
                    txtAddress1.Text = drHarasee("Line1")
                    txtAddress2.Text = drHarasee("Line2")
                    If rdgZip.SelectedValue = "1" Then
                        txtSearchZip.Text = drHarasee("Primary_City")
                    Else
                        txtSearchZip.Text = drHarasee("ZIP")
                    End If
                    txtCity.Text = drHarasee("Primary_City")
                    txtState.Text = drHarasee("State")
                    txtZipResult.Text = drHarasee("ZIP")
                    txtWorkPhone.Text = drHarasee("WorkPhone")
                    txtCellPhone.Text = drHarasee("CellPhone")
                    txtPrimaryEmail.Text = drHarasee("PrimaryEmailID")
                    txtAlternateEmail.Text = drHarasee("AlternateEmailID")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub clearControls()
        Try
            hdnContactID.Value = ""
            txtFName.Text = ""
            txtMName.Text = ""
            txtLName.Text = ""
            txtJobTitle.Text = ""
            txtAddress1.Text = ""
            txtAddress2.Text = ""
            txtSearchZip.Text = ""
            txtCity.Text = ""
            txtState.Text = ""
            txtZipResult.Text = ""
            txtWorkPhone.Text = ""
            txtCellPhone.Text = ""
            txtPrimaryEmail.Text = ""
            txtAlternateEmail.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DeleteContact(ByVal ContactID As Int64)
        Try
            DBConn.ExecuteStatement("UPDATE dbo.Address SET Row_Delete = 1 WHERE EntityID = " & ContactID)
            DBConn.ExecuteStatement("UPDATE dbo.Contact SET Row_Delete = 1 WHERE ContactID = " & ContactID)
            LoadValues()
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btnAddContact_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddContact.Click
        Try
            clearControls()
            ViewState("Mode") = "Add"
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()

            If ViewState("Mode") = "Add" Then
                cmd.CommandText = "spAddHarasser"
            ElseIf ViewState("Mode") = "Update" Then
                cmd.CommandText = "spUpdateHarasser"
            End If

            cmd.CommandType = CommandType.StoredProcedure

            If ViewState("Mode") = "Add" Then
                param = New SqlClient.SqlParameter()
                param.ParameterName = "@ComplaintID"
                param.DbType = DbType.Int64
                param.Value = Session("ComplaintID")
                cmd.Parameters.Add(param)
            ElseIf ViewState("Mode") = "Update" Then
                param = New SqlClient.SqlParameter()
                param.ParameterName = "@ContactID"
                param.DbType = DbType.Int64
                param.Value = hdnContactID.Value
                cmd.Parameters.Add(param)
            End If

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@FirstName"
            param.DbType = DbType.String
            param.Value = IIf(txtFName.Text.Trim() <> String.Empty, txtFName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@MiddleName"
            param.DbType = DbType.String
            param.Value = IIf(txtMName.Text.Trim() <> String.Empty, txtMName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@LastName"
            param.DbType = DbType.String
            param.Value = IIf(txtLName.Text.Trim() <> String.Empty, txtLName.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@JobTitle"
            param.DbType = DbType.String
            param.Value = IIf(txtJobTitle.Text.Trim() <> String.Empty, txtJobTitle.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Line1"
            param.DbType = DbType.String
            param.Value = IIf(txtAddress1.Text.Trim() <> String.Empty, txtAddress1.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Line2"
            param.DbType = DbType.String
            param.Value = IIf(txtAddress2.Text.Trim() <> String.Empty, txtAddress2.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Zip"
            param.DbType = DbType.Int32
            param.Value = IIf(txtSearchZip.Text.Trim() <> String.Empty, txtSearchZip.Text.Trim(), System.DBNull.Value) 'txtZip.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@CellPhone"
            param.DbType = DbType.String
            param.Value = IIf(txtCellPhone.Text.Trim() <> String.Empty, txtCellPhone.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@WorkPhone"
            param.DbType = DbType.String
            param.Value = IIf(txtWorkPhone.Text.Trim() <> String.Empty, txtWorkPhone.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@PrimaryEmailID"
            param.DbType = DbType.String
            param.Value = IIf(txtPrimaryEmail.Text.Trim() <> String.Empty, txtPrimaryEmail.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@AlternateEmailID"
            param.DbType = DbType.String
            param.Value = IIf(txtAlternateEmail.Text.Trim() <> String.Empty, txtAlternateEmail.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)

            If Not dtItems Is Nothing Then
                ContactID = dtItems.Rows(0)("ContactID")
                LoadValues()
                clearControls()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtSearchZip_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchZip.TextChanged
        Dim ZipAscs As String()
        Try

            If txtSearchZip.Text.Trim().Length > 0 AndAlso Not txtSearchZip.Text.Trim().Contains("Enter") Then
                ZipAscs = txtSearchZip.Text.Trim.Split("/")
            End If

            If Not ZipAscs Is Nothing AndAlso ZipAscs.Count > 2 Then
                If rdgZip.SelectedValue = "1" Then
                    txtSearchZip.Text = ZipAscs(1).Trim()
                Else
                    txtSearchZip.Text = ZipAscs(0).Trim()
                End If
                txtZipResult.Text = ZipAscs(0).Trim()
                txtCity.Text = ZipAscs(1).Trim()
                txtState.Text = ZipAscs(2).Trim()
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class