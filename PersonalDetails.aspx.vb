﻿Imports WF.WatchForce.Data
Imports System.Data.SqlClient

Partial Public Class PersonalDetails
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql
    Dim ComplaintID, ContactID As Int64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")

            CalExtDOB.EndDate = DateTime.Today

            If Not Page.IsPostBack Then

                LoadAncestrTypes()
                'ddlBranches.Items.Add("Please Select")
                'populateZip()

            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub LoadAncestrTypes()
        Dim cmd As SqlClient.SqlCommand
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "sp_getAncestryTypes"
            cmd.CommandType = CommandType.StoredProcedure

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                ddlAncestry.DataSource = dtItems
                ddlAncestry.DataValueField = "Code"
                ddlAncestry.DataTextField = "Description"
                ddlAncestry.DataBind()
            End If

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Try
            SaveComplaintContact()
            Response.Redirect("ComplaintDetails.aspx", False)
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Private Sub SaveComplaintContact()
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "sp_AddPersonalInfo"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@BranchID"
            param.DbType = DbType.Int64
            param.Value = Session("BranchID")
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@FirstName"
            param.DbType = DbType.String
            param.Value = txtFName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@MiddleName"
            param.DbType = DbType.String
            param.Value = txtMName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@LastName"
            param.DbType = DbType.String
            param.Value = txtLName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@PrefixTypeCode"
            param.DbType = DbType.Int32
            param.Value = ddlPrefix.SelectedValue
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@GenderTypeCode"
            param.DbType = DbType.Int32
            param.Value = rdgGender.SelectedValue
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DateOfBirth"
            param.DbType = DbType.Date
            param.Value = IIf(txtDOB.Text.Trim() <> String.Empty, txtDOB.Text.Trim(), "1/1/1900")
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@AGE"
            param.DbType = DbType.Int32
            param.Value = IIf(txtAge.Text.Trim() <> String.Empty, txtAge.Text.Trim(), System.DBNull.Value)
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@AncestryTypeCode"
            param.DbType = DbType.Int32
            param.Value = ddlAncestry.SelectedValue
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@AncestryTypeOthers"
            param.DbType = DbType.String
            param.Value = txtOtherAncestry.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Line1"
            param.DbType = DbType.String
            param.Value = txtAddress1.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Line2"
            param.DbType = DbType.String
            param.Value = txtAddress2.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Zip"
            param.DbType = DbType.Int32
            param.Value = txtZip.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@HomePhone"
            param.DbType = DbType.String
            param.Value = txtHomePhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@CellPhone"
            param.DbType = DbType.String
            param.Value = txtCellPhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@WorkPhone"
            param.DbType = DbType.String
            param.Value = txtWorkPhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@PrimaryEmailID"
            param.DbType = DbType.String
            param.Value = txtPrimaryEmail.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@AlternateEmailID"
            param.DbType = DbType.String
            param.Value = txtAlternateEmail.Text.Trim()
            cmd.Parameters.Add(param)
            
            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)

            If Not dtItems Is Nothing Then
                ComplaintID = dtItems.Rows(0)("ComplaintID")
                Session("ComplaintID") = ComplaintID
                ContactID = dtItems.Rows(0)("ContactID")
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub populateZip()
        Dim strSql As String
        Dim objCmd As SqlClient.SqlCommand
        Try


            strSql = "SELECT " & _
                     "ISNULL(CONVERT(VARCHAR,ZIP),'') + '  ' + " & _
                     "ISNULL(CONVERT(VARCHAR,Primary_City),'') + '  ' + " & _
                     "ISNULL(CONVERT(VARCHAR,State),'') ZipFull " & _
                                "FROM DBO.ZIPCODE " & _
                     "WHERE ZIP LIKE @ZIP " & _
                     "ORDER BY ZIP"

            objCmd = New SqlClient.SqlCommand()
            objCmd.Connection = New SqlClient.SqlConnection(DBConn.DBConnection)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = strSql
            objCmd.Parameters.AddWithValue("@ZIP", "890" + "%")

            Dim objAdapter As New SqlDataAdapter()
            dtItems = New DataTable()

            objAdapter.SelectCommand = objCmd
            'objAdapter.SelectCommand.Connection = mobjConnection
            objAdapter.SelectCommand.CommandTimeout = 0
            objAdapter.Fill(dtItems)
            objAdapter.Dispose()
            objAdapter = Nothing

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    Sub CheckPhones(ByVal source As Object, ByVal args As ServerValidateEventArgs)

        Try
            If (txtHomePhone.Text = "") Then
                args.IsValid = False
                Exit Sub
            End If

            args.IsValid = True
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try

    End Sub

    Protected Sub zipSelected(ByVal sender As Object, ByVal e As EventArgs) Handles txtZip.TextChanged
        Dim strSql As String
        Dim objCmd As SqlClient.SqlCommand
        Dim strZip As String
        Try
            If (txtZip.Text.Trim() <> String.Empty) Then

                strZip = txtZip.Text.Trim().Substring(0, txtZip.Text.Trim().IndexOf(" "))

                strSql = "SELECT ISNULL(CONVERT(VARCHAR,ZIP),'') ZIP, ISNULL(CONVERT(VARCHAR,Primary_City),'') Primary_City, ISNULL(CONVERT(VARCHAR,State),'') State FROM DBO.ZIPCODE WHERE ZIP = " & strZip

                objCmd = New SqlClient.SqlCommand()
                objCmd.Connection = New SqlClient.SqlConnection(DBConn.DBConnection)
                objCmd.CommandType = CommandType.Text
                objCmd.CommandText = strSql

                Dim objAdapter As New SqlDataAdapter()
                dtItems = New DataTable()

                objAdapter.SelectCommand = objCmd
                objAdapter.SelectCommand.CommandTimeout = 0
                objAdapter.Fill(dtItems)
                objAdapter.Dispose()
                objAdapter = Nothing

                If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                    lblCity.Text = dtItems.Rows(0)("Primary_City")
                    lblState.Text = dtItems.Rows(0)("State")
                    lblZip.Text = dtItems.Rows(0)("ZIP")
                End If
            Else
                lblCity.Text = "-"
                lblState.Text = "-"
                lblZip.Text = "-"
            End If

        Catch ex As Exception
            Console.Write(ex.Message)
            'ObjError.LogError(ex)
        End Try
    End Sub

    <System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()> Public Shared Function SearchZip(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim strSql As String
        Dim objCmd As SqlClient.SqlCommand
        Dim dtItems As DataTable
        Try


            strSql = "SELECT " & _
                     "ISNULL(CONVERT(VARCHAR,ZIP),'') + '  ' + " & _
                     "ISNULL(CONVERT(VARCHAR,Primary_City),'') + '  ' + " & _
                     "ISNULL(CONVERT(VARCHAR,State),'') ZipFull " & _
                                "FROM DBO.ZIPCODE " & _
                     "WHERE ZIP LIKE @ZIP " & _
                     "ORDER BY ZIP"

            objCmd = New SqlClient.SqlCommand()
            objCmd.Connection = New SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("WFConnString").ConnectionString)
            objCmd.CommandType = CommandType.Text
            objCmd.CommandText = strSql
            objCmd.Parameters.AddWithValue("@ZIP", prefixText + "%")

            Dim objAdapter As New SqlDataAdapter()
            dtItems = New DataTable()

            objAdapter.SelectCommand = objCmd
            'objAdapter.SelectCommand.Connection = mobjConnection
            objAdapter.SelectCommand.CommandTimeout = 0
            objAdapter.Fill(dtItems)
            objAdapter.Dispose()
            objAdapter = Nothing

            Dim Zips As List(Of String) = New List(Of String)
            For Each dtrow In dtItems.Rows
                Zips.Add(dtrow("ZipFull").ToString)
            Next

            Return Zips

        Catch ex As Exception
            Console.Write(ex.Message)
            'ObjError.LogError(ex)
        End Try

    End Function

    
End Class