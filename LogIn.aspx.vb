﻿Imports WF.WatchForce.Data
Imports System.Data.SqlClient

Partial Public Class LogIn
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql
    Dim UserID As Int64


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")

            If Not Page.IsPostBack Then
                
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

    
    Protected Sub btnSignIn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSignIn.Click
        Try
            CheckLogIn()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CheckLogIn()
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter

        Try

            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spAuthenticateUser"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@vUserName"
            param.DbType = DbType.String
            param.Value = txtUserID.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@vPassword"
            param.DbType = DbType.String
            param.Value = txtPassCode.Text()
            cmd.Parameters.Add(param)

            'param = New SqlClient.SqlParameter()
            'param.ParameterName = "@vUserID"
            'param.DbType = DbType.Int64
            'param.Direction = ParameterDirection.Output
            'cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                UserID = dtItems.Rows(0)("UserID")

                Session("UserID") = UserID

                'Page.ClientScript.RegisterStartupScript(Me.GetType(), "ExistErr", _
                '            "<script language=""javascript"" type=""text/javascript"">" & vbNewLine & _
                '            "alert('Authentication Accepted\n For Sandeep, UserId :" & UserID & "');" & vbNewLine & _
                '            "</script>")

                Response.Redirect("SelectORG.aspx", False)

            Else

                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ExistErr", _
                            "<script language=""javascript"" type=""text/javascript"">" & vbNewLine & _
                            "alert('Incorrect UserId or Password ..!\nPlease try again.');" & vbNewLine & _
                            "</script>")
            End If

        Catch ex As Exception
            ObjError.LogError(ex)
        End Try
    End Sub

End Class