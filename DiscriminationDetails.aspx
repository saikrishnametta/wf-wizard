<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/WatchForce.Master" CodeBehind="DiscriminationDetails.aspx.vb" Inherits="WF.DiscriminationDetails" 
    title="Discrimination Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder"  runat="server">

<cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc1:ToolkitScriptManager> 

<script type="text/javascript" id="validate" language="javascript">

    function enableDisabilityDep() {

        //alert(document.getElementById('<%=chkDisability.ClientID%>').checked);

        if (document.getElementById('<%=chkDisability.ClientID%>').checked) {

            document.getElementById('<%=ddlDisabilityHistory.ClientID%>').disabled = false;
            document.getElementById('<%=ddlWorkingConditions.ClientID%>').disabled = false;
            document.getElementById('<%=ddlMedicalRecords.ClientID%>').disabled = false;
            document.getElementById('<%=txtReqToFName.ClientID%>').disabled = false;
            document.getElementById('<%=txtReqToLName.ClientID%>').disabled = false;            
        }
        else {

            document.getElementById('<%=ddlDisabilityHistory.ClientID%>').disabled = true;
            document.getElementById('<%=ddlDisabilityHistory.ClientID%>').options[0].selected = true;
            document.getElementById('<%=ddlWorkingConditions.ClientID%>').disabled = true;
            document.getElementById('<%=ddlWorkingConditions.ClientID%>').options[0].selected = true;
            document.getElementById('<%=ddlMedicalRecords.ClientID%>').disabled = true;
            document.getElementById('<%=ddlMedicalRecords.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtReqToFName.ClientID%>').disabled = true;
            document.getElementById('<%=txtReqToFName.ClientID%>').value = ""
            document.getElementById('<%=txtReqToLName.ClientID%>').disabled = true;
            document.getElementById('<%=txtReqToLName.ClientID%>').value = "";            
        }

    }


    function enableChkRaceDep() {

        //alert(document.getElementById('<%=chkRace.ClientID%>').checked);

        if (document.getElementById('<%=chkRace.ClientID%>').checked) {
            document.getElementById('<%=ddlRace.ClientID%>').disabled = false;            
            //document.getElementById('<%=txtRaceOther.ClientID%>').disabled = false;            
        }
        else {

            document.getElementById('<%=ddlRace.ClientID%>').disabled = true;
            document.getElementById('<%=ddlRace.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtRaceOther.ClientID%>').disabled = true;
            document.getElementById('<%=txtRaceOther.ClientID%>').value = "";            
        }

    }
    
    function enableOtherRace() {

        //alert(document.getElementById('<%=ddlRace.ClientID%>').value);

        if (document.getElementById('<%=ddlRace.ClientID%>').value == "56") {
            document.getElementById('<%=txtRaceOther.ClientID%>').disabled = false;
            document.getElementById('<%=txtRaceOther.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtRaceOther.ClientID%>').disabled = true;
            document.getElementById('<%=txtRaceOther.ClientID%>').value = "";
        }
    }

    function enablechkNatOriginDep() {

        //alert(document.getElementById('<%=chkNatOrigin.ClientID%>').checked);

        if (document.getElementById('<%=chkNatOrigin.ClientID%>').checked) {
            document.getElementById('<%=ddlNatOrigin.ClientID%>').disabled = false;
            //document.getElementById('<%=txtNatOriginOthers.ClientID%>').disabled = false;            
        }
        else {

            document.getElementById('<%=ddlNatOrigin.ClientID%>').disabled = true;
            document.getElementById('<%=ddlNatOrigin.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtNatOriginOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtNatOriginOthers.ClientID%>').value = "";
        }

    }

    function enableOtherNatOrigin() {

        //alert(document.getElementById('<%=ddlNatOrigin.ClientID%>').value);

        if (document.getElementById('<%=ddlNatOrigin.ClientID%>').value == "61") {
            document.getElementById('<%=txtNatOriginOthers.ClientID%>').disabled = false;
            document.getElementById('<%=txtNatOriginOthers.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtNatOriginOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtNatOriginOthers.ClientID%>').value = "";
        }
    }
  
    function enablechkSexOrientationDep() {
        //alert(document.getElementById('<%=chkSexOrientation.ClientID%>').checked);
        if (document.getElementById('<%=chkSexOrientation.ClientID%>').checked) {
            document.getElementById('<%=ddlSexOrientation.ClientID%>').disabled = false;
        }
        else {
            document.getElementById('<%=ddlSexOrientation.ClientID%>').disabled = true;
            document.getElementById('<%=ddlSexOrientation.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtSexOrientationOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtSexOrientationOthers.ClientID%>').value = ""; 
        }
    }

    function enableSexOrientationOther() {

        //alert(document.getElementById('<%=ddlSexOrientation.ClientID%>').value);

        if (document.getElementById('<%=ddlSexOrientation.ClientID%>').value == "163") {
            document.getElementById('<%=txtSexOrientationOthers.ClientID%>').disabled = false;
            document.getElementById('<%=txtSexOrientationOthers.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtSexOrientationOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtSexOrientationOthers.ClientID%>').value = "";
        }
    }

    function enablechkGenderIdentityDep() {
        //alert(document.getElementById('<%=chkGenderIdentity.ClientID%>').checked);
        if (document.getElementById('<%=chkGenderIdentity.ClientID%>').checked) {
            document.getElementById('<%=ddlGenderIdentity.ClientID%>').disabled = false;
        }
        else {
            document.getElementById('<%=ddlGenderIdentity.ClientID%>').disabled = true;
            document.getElementById('<%=ddlGenderIdentity.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtGenderIdentityOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtGenderIdentityOthers.ClientID%>').value = ""; 
        }
    }

    function enableGenderIdentityOther() {

        //alert(document.getElementById('<%=ddlGenderIdentity.ClientID%>').value);

        if (document.getElementById('<%=ddlGenderIdentity.ClientID%>').value == "162") {
            document.getElementById('<%=txtGenderIdentityOthers.ClientID%>').disabled = false;
            document.getElementById('<%=txtGenderIdentityOthers.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtGenderIdentityOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtGenderIdentityOthers.ClientID%>').value = "";
        }
    }
    
    function enablechkPregnancyDep() {
        //alert(document.getElementById('<%=chkPregnancy.ClientID%>').checked);
        if (document.getElementById('<%=chkPregnancy.ClientID%>').checked) {
            document.getElementById('<%=ddlPregnancy.ClientID%>').disabled = false;
        }
        else {
            document.getElementById('<%=ddlPregnancy.ClientID%>').disabled = true;
            document.getElementById('<%=ddlPregnancy.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtPregnancyOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtPregnancyOthers.ClientID%>').value = ""; 
        }
    }

    function enablePregnancyOther() {

        //alert(document.getElementById('<%=ddlPregnancy.ClientID%>').value);

        if (document.getElementById('<%=ddlPregnancy.ClientID%>').value == "164") {
            document.getElementById('<%=txtPregnancyOthers.ClientID%>').disabled = false;
            document.getElementById('<%=txtPregnancyOthers.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtPregnancyOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtPregnancyOthers.ClientID%>').value = "";
        }
    }
    

    function enablechkReligionDep() {
        //alert(document.getElementById('<%=chkReligion.ClientID%>').checked);
        if (document.getElementById('<%=chkReligion.ClientID%>').checked) {
            document.getElementById('<%=ddlReligion.ClientID%>').disabled = false;
        }
        else {
            document.getElementById('<%=ddlReligion.ClientID%>').disabled = true;
            document.getElementById('<%=ddlReligion.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtReligionOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtReligionOthers.ClientID%>').value = "";
        }
    }

    function enableReligionOther() {

        //alert(document.getElementById('<%=ddlReligion.ClientID%>').value);

        if (document.getElementById('<%=ddlReligion.ClientID%>').value == "165") {
            document.getElementById('<%=txtReligionOthers.ClientID%>').disabled = false;
            document.getElementById('<%=txtReligionOthers.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtReligionOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtReligionOthers.ClientID%>').value = "";
        }
    }

    function enablechkColorDep() {
        //alert(document.getElementById('<%=chkColor.ClientID%>').checked);
        if (document.getElementById('<%=chkColor.ClientID%>').checked) {
            document.getElementById('<%=ddlColor.ClientID%>').disabled = false;
        }
        else {
            document.getElementById('<%=ddlColor.ClientID%>').disabled = true;
            document.getElementById('<%=ddlColor.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtColorOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtColorOthers.ClientID%>').value = "";
        }
    }

    function enableColorOther() {

        //alert(document.getElementById('<%=ddlColor.ClientID%>').value);

        if (document.getElementById('<%=ddlColor.ClientID%>').value == "166") {
            document.getElementById('<%=txtColorOthers.ClientID%>').disabled = false;
            document.getElementById('<%=txtColorOthers.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtColorOthers.ClientID%>').disabled = true;
            document.getElementById('<%=txtColorOthers.ClientID%>').value = "";
        }
    }

    function enablechkRetaliationDep() {

        //alert(document.getElementById('<%=chkRetaliation.ClientID%>').checked);

        if (document.getElementById('<%=chkRetaliation.ClientID%>').checked) {
            document.getElementById('<%=ddlRetaliation.ClientID%>').disabled = false;
            //document.getElementById('<%=txtRetaliationOther.ClientID%>').disabled = false;            
        }
        else {

            document.getElementById('<%=ddlRetaliation.ClientID%>').disabled = true;
            document.getElementById('<%=ddlRetaliation.ClientID%>').options[0].selected = true;

            document.getElementById('<%=txtRetaliationOther.ClientID%>').disabled = true;
            document.getElementById('<%=txtRetaliationOther.ClientID%>').value = "";
        }

    }

    function enableRetaliationOther() {

        //alert(document.getElementById('<%=ddlRetaliation.ClientID%>').value);

        if (document.getElementById('<%=ddlRetaliation.ClientID%>').value == "160") {
            document.getElementById('<%=txtRetaliationOther.ClientID%>').disabled = false;
            document.getElementById('<%=txtRetaliationOther.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtRetaliationOther.ClientID%>').disabled = true;
            document.getElementById('<%=txtRetaliationOther.ClientID%>').value = "";
        }
    }

    function enablechkDesrReasonNotListedDep() {
        //alert(document.getElementById('<%=chkDesrReasonNotListed.ClientID%>').checked);
        if (document.getElementById('<%=chkDesrReasonNotListed.ClientID%>').checked) {
            document.getElementById('<%=txtDesrOtherReason.ClientID%>').disabled = false;
            document.getElementById('<%=txtDesrOtherReason.ClientID%>').focus();
        }
        else {
            document.getElementById('<%=txtDesrOtherReason.ClientID%>').disabled = true;
            document.getElementById('<%=txtDesrOtherReason.ClientID%>').value = "";
        }
    }

    function pageLoad(sender, args) {
        //alert('Page Load called');
        enableDisabilityDep();
        enableChkRaceDep();
        enableOtherRace();
        enablechkNatOriginDep();
        enableOtherNatOrigin();
        enablechkSexOrientationDep();
        enableSexOrientationOther();
        enablechkGenderIdentityDep();
        enableGenderIdentityOther();
        enablechkPregnancyDep();
        enablePregnancyOther();
        enablechkReligionDep();
        enableReligionOther();
        enablechkColorDep();
        enableColorOther();
        enablechkRetaliationDep();
        enableRetaliationOther();
        enablechkDesrReasonNotListedDep();
    }
       
</script>

    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="90%">
     <tr>
        <td>            
            <asp:Label ID="lblHeader" Font-Bold="true" Font-Size="Medium" runat="server" CssClass="accordionHeader" Text="Discrimination Details"></asp:Label>
        </td>
     </tr>
    </table>
    
    <table border="0" style="border-color:Silver;" cellpadding="0" cellspacing="0"  align="center" width="90%">
      
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td>
                
        <table border="0" width="100%">   
        <tr>
            <td>
                <table width="100%" border= "0">
                    <tr>
                        <td align="left" valign="top" colspan="4" class="lblContent">
                            Date of most recent discriminatory incident &nbsp;&nbsp;
                            <asp:TextBox ID="txtRecentDscrDate" runat="server" style="width:100px;"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarRecentDesDt" 
                                TargetControlID="txtRecentDscrDate" runat="server">
                            </cc1:CalendarExtender>                            
                        </td>  
                    </tr>
                    <tr>                        
                        <td  align="left" valign="top" class="lblContent" colspan="4">
                            <u style="text-underline:single">
                             Check the reason for your discrimination</u>(I feel discriminated because of my�)
                        </td> 
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="4" >
                            &nbsp;
                        </td>   
                    </tr>
                    <tr>
                        <td align="left" valign="middle" style="width:3%">
                           <asp:CheckBox ID="chkRace" runat="server" onclick="enableChkRaceDep();"/>                            
                         </td>
                        <td style="width:25%" class="lblContent" valign="middle">Race</td>
                        <td align="left" style="width:20%" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlRace" onchange="enableOtherRace();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>                                
                            </asp:DropDownList>
                        &nbsp;</td>   
                        <td style="width:72%" class="lblContent" valign="top">
                            If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtRaceOther" runat="server" Enabled="false">
                            </asp:TextBox>                            
                        </td>    
                    </tr>                    
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkNatOrigin" runat="server" onclick="enablechkNatOriginDep();" />                            
                         </td>
                        <td class="lblContent" valign="middle">National Origin</td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlNatOrigin" onchange="enableOtherNatOrigin();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                            If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtNatOriginOthers" runat="server" Enabled="false">
                            </asp:TextBox>                            
                        </td>    
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkGender" runat="server" />                            
                         </td>
                        <td class="lblContent" valign="middle">Gender</td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:TextBox ID="txtGender" runat="server" Enabled="false" Width="80%"></asp:TextBox>
                            <%--<asp:DropDownList ID="ddlSex" Enabled="false" runat="server">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>--%>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                            <%--If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtSexOthers" runat="server" Enabled="false">
                            </asp:TextBox>   --%>                         
                        </td>    
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkGenderIdentity" onclick="enablechkGenderIdentityDep();" runat="server" />                            
                         </td>
                        <td class="lblContent" valign="middle">Gender identity</td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlGenderIdentity" onchange="enableGenderIdentityOther();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                           If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtGenderIdentityOthers" runat="server" Enabled="false">
                            </asp:TextBox>                        
                        </td>    
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkSexOrientation" runat="server" onclick="enablechkSexOrientationDep();" />                            
                         </td>
                        <td class="lblContent" valign="middle">Sex Orientation</td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlSexOrientation" onchange="enableSexOrientationOther();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                            If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtSexOrientationOthers" runat="server" Enabled="false">
                            </asp:TextBox>         
                        </td>    
                    </tr>
                    
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkPregnancy" runat="server" onclick="enablechkPregnancyDep();"/>                            
                         </td>
                        <td class="lblContent" valign="middle">Pregnancy</td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlPregnancy" onchange="enablePregnancyOther();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                            If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtPregnancyOthers" runat="server" Enabled="false">
                            </asp:TextBox>          
                        </td>    
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkReligion" runat="server" onclick="enablechkReligionDep();"/>                            
                         </td>
                        <td class="lblContent" valign="middle">Religion/Creed</td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlReligion" onchange="enableReligionOther();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                            If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtReligionOthers" runat="server" Enabled="false">
                            </asp:TextBox>                            
                        </td>    
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkColor" runat="server" onclick="enablechkColorDep();"/>                            
                         </td>
                        <td class="lblContent" valign="middle">Color</td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlColor" onchange="enableColorOther();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                            If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtColorOthers" runat="server" Enabled="false">
                            </asp:TextBox>                            
                        </td>    
                    </tr>
                    <tr>
                       <td align="left" valign="middle">
                           <asp:CheckBox ID="chkAge" runat="server" />                            
                         </td>
                        <td class="lblContent" colspan="3">
                            Age
                        </td>
                    </tr>
                    <tr>
                       <td align="left" valign="middle">
                           <asp:CheckBox ID="chkRetaliation" runat="server" onclick="enablechkRetaliationDep();"/>                           
                         </td>
                        <td class="lblContent">
                            Retaliation
                        </td>
                        <td align="left" valign="top" class="lblContent">
                            <asp:DropDownList ID="ddlRetaliation" onchange="enableRetaliationOther();" Enabled="false" runat="server" Width="80%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        &nbsp;</td>   
                        <td class="lblContent" valign="top">
                            If Others, then specify &nbsp;&nbsp;
                            <asp:TextBox ID="txtRetaliationOther" runat="server" Enabled="false">
                            </asp:TextBox>                            
                        </td>   
                    </tr>
                    <tr>
                       <td align="left" valign="middle">
                           <asp:CheckBox ID="chkDisability" onclick="enableDisabilityDep();" runat="server" />                            
                         </td>
                        <td class="lblContent" colspan="3">
                            Disability
                        </td>
                    </tr>
                    <tr>
                       <td align="left" valign="middle">                           
                         </td>
                        <td class="lblContent" >
                            Disability History
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlDisabilityHistory" Enabled="false" runat="server">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                       <td align="left" valign="middle">                           
                         </td>
                        <td class="lblContent" >
                            Did you ask the employer for any assistance or change in working conditions because of your disability?
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlWorkingConditions" Enabled="false" runat="server" Width="22%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                       <td align="left" valign="middle">                           
                         </td>
                        <td class="lblContent" >
                               Were medical records submitted ?
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlMedicalRecords" Enabled="false" runat="server" Width="22%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                       <td align="left" valign="middle">                           
                         </td>
                        <td class="lblContent" valign="top" >
                            Who did you make this request to ?
                        </td>
                        <td colspan="2">
                            <table style="width:100%" border="0">
                                <tr>
                                    <td class="lblContent" style="width:25%" >First Name</td>
                                    <td class="lblContent" style="width:75%">Last Name</td>
                                </tr>
                                <tr>
                                    <td class="lblContent" >
                                        <asp:TextBox ID="txtReqToFName" Enabled="false" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="lblContent">
                                        <asp:TextBox ID="txtReqToLName" Enabled="false" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                       <td align="left" valign="middle">                           
                         </td>
                        <td class="lblContent" >
                               How did you make this request ?
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlHowRequestMade" runat="server" Width="22%">
                                <asp:ListItem Text="Please Select" Value="-1"></asp:ListItem>
                             </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2" class="lblContent">
                            Please describe in 250 words or less, the assistance or change in working conditions you requested, and the result of your request?
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2" class="lblContent">
                            <asp:TextBox ID="txtWorkCondionChanges" Width="90%" runat="server" TextMode="MultiLine" Rows="3" ></asp:TextBox>
                          <%-- <cc1:RoundedCornersExtender ID="rndExtWorkCondiChanges" TargetControlID="txtWorkCondionChanges" Corners="All" Radius="9" runat="server">
                            </cc1:RoundedCornersExtender>--%>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                           <asp:CheckBox ID="chkDesrReasonNotListed" runat="server" onclick="enablechkDesrReasonNotListedDep();"/>                            
                         </td>
                        <td class="lblContent" colspan="3" valign="middle">
                            Reason for discrimination is not  listed here
                        </td>                        
                    </tr>
                    <tr>
                        <td align="left" valign="middle">                           
                         </td>
                        <td class="lblContent" colspan="2" valign="middle">
                            Please explain the reason for discrimination in the box below
                        </td>     
                        <td colspan="2"></td>                   
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2" class="lblContent">
                            <asp:TextBox ID="txtDesrOtherReason" runat="server" Width="90%" Enabled="false" TextMode="MultiLine" Rows="3" ></asp:TextBox>
                            <%--<cc1:RoundedCornersExtender ID="RoundedCornersExtender1" TargetControlID="txtDesrOtherReason" Corners="All" Radius="9" runat="server">
                            </cc1:RoundedCornersExtender>--%>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    
                 </table>
            </td>           
            
        </tr>
      </table>
        
          
        </td>
      </tr>
     
    </table>  
    
    
    
    <table border = "0" width="90%">  
      <%--<tr>
        <td colspan="2"><br /></td>
      </tr> --%>     
      <tr>      
      <td align="right" style="width:92%">
          <asp:Button ID="btnProceed" CssClass="btnstyle" runat="server" Text="Save & Continue" Width="140px" Height="25px" />
          <cc1:RoundedCornersExtender ID="RoundedCornersExtender2" TargetControlID="btnProceed" Corners="All" Radius="6" runat="server">
              </cc1:RoundedCornersExtender>
          </td>
      <td style="width:8%"></td>
      </tr>
      <tr><td colspan="2">&nbsp;</td></tr>
      </table>  
</asp:Content>


