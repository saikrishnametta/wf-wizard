﻿Imports WF.WatchForce.Data

Partial Public Class ComplainantInfo
    Inherits System.Web.UI.Page

    Dim ObjError As EventLogger
    Dim dtItems As DataTable
    Dim DBConn As DBSql

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("UserID") = Nothing) Then
                Server.Transfer("TimeOut.aspx", False)
            End If

            ObjError = New EventLogger()
            DBConn = New DBSql("sa")
            'lblDate.text = "Creation date: " & Date.Now.ToString()

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveComplaintContact()
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spAddContact"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@FirstName"
            param.DbType = DbType.String
            param.Value = txtFName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@MiddleName"
            param.DbType = DbType.String
            param.Value = txtMName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@LastName"
            param.DbType = DbType.String
            param.Value = txtLName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@PrefixCode"
            param.DbType = DbType.Int32
            param.Value = ddlPrefix.SelectedValue
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@DateOfBirth"
            param.DbType = DbType.Date
            param.Value = "01/01/1900"
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Address"
            param.DbType = DbType.String
            param.Value = txtAddress.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@City"
            param.DbType = DbType.String
            param.Value = txtCity.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@State"
            param.DbType = DbType.String
            param.Value = txtState.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Zip"
            param.DbType = DbType.String
            param.Value = txtZip.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@CountryCode"
            param.DbType = DbType.String
            param.Value = txtCountry.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Email"
            param.DbType = DbType.String
            param.Value = txtEmail.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@HomePhone"
            param.DbType = DbType.String
            param.Value = txtHomePhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@CellPhone"
            param.DbType = DbType.String
            param.Value = txtCellPhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@WorkPhone"
            param.DbType = DbType.String
            param.Value = txtWorkPhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ContactTypeCode"
            param.DbType = DbType.Int32
            param.Value = "1"
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)

            If Not dtItems Is Nothing Then
                Dim contactID As Integer
                contactID = dtItems.Rows(0)("ContactID")
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Try
            SaveComplaintContact()
            'SaveAttorneyContact()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveAttorneyContact()
        Dim cmd As SqlClient.SqlCommand
        Dim param As SqlClient.SqlParameter
        Try
            dtItems = New DataTable()
            cmd = New SqlClient.SqlCommand()
            cmd.CommandText = "spAddContact"
            cmd.CommandType = CommandType.StoredProcedure

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@FirstName"
            param.DbType = DbType.String
            param.Value = txtAtrnFName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@MiddleName"
            param.DbType = DbType.String
            param.Value = txtAtrnMName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@LastName"
            param.DbType = DbType.String
            param.Value = txtAtrnLName.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Address"
            param.DbType = DbType.String
            param.Value = txtAtrnAddress.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@City"
            param.DbType = DbType.String
            param.Value = txtAtrnCity.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@State"
            param.DbType = DbType.String
            param.Value = txtAtrnState.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Zip"
            param.DbType = DbType.String
            param.Value = txtAtrnZip.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@CountryCode"
            param.DbType = DbType.String
            param.Value = txtAtrnCountry.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@Email"
            param.DbType = DbType.String
            param.Value = txtAtrnEmail.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@CellPhone"
            param.DbType = DbType.String
            param.Value = txtAtrnCellPhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@WorkPhone"
            param.DbType = DbType.String
            param.Value = txtAtrnWorkPhone.Text.Trim()
            cmd.Parameters.Add(param)

            param = New SqlClient.SqlParameter()
            param.ParameterName = "@ContactTypeCode"
            param.DbType = DbType.Int32
            param.Value = "2"
            cmd.Parameters.Add(param)

            If Not DBConn Is Nothing Then DBConn.getDataTableFromSP(cmd, dtItems)

            If Not dtItems Is Nothing Then
                Dim AtrncontactID As Integer
                AtrncontactID = dtItems.Rows(0)("ContactID")
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class